﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.IO.Packaging;
using Newtonsoft.Json;
using System.Threading;
using System.Xml.Serialization;
using Microsoft.Office.Interop.Visio;
using Aspose.Diagram;
using VisioLibrary;
using System.Configuration;

namespace Visio
{
    public partial class frmVisio : Form
    {
        public frmVisio()
        {
            InitializeComponent();            
        }

        public int editRecord = 0;
        public const string getFilePath = "File";
        public const string getFolderPath = "Folder";
        private void frmVisio_Load(object sender, EventArgs e)
        {
            string strMessage = checkAppConfig();
            if (strMessage != string.Empty)
            {
                MessageBox.Show(strMessage, "App Config Setting");
                System.Windows.Forms.Application.Exit();
            }
            BindList();
        }
        
        public string checkAppConfig()
        {
            string strMessage = string.Empty;
            string checkCSV = string.Empty;
            string config =  ConfigurationManager.AppSettings["Resource_Mapping"];
            checkCSV = config.Substring(config.Length - 4);
            if (config == string.Empty)
            {
                strMessage = "Please fill Resource_Mapping Path \n";
            }
            else if(checkCSV.ToLower() != ".csv")
            {
                strMessage += "Please fill only csv File for Resource_Mapping \n";
            }
            config = ConfigurationManager.AppSettings["Resource_Text"];
            checkCSV = config.Substring(config.Length - 4);
            if (config == string.Empty)
            {
                strMessage += "Please fill Resource_Text Path \n";
            }
            else if (checkCSV.ToLower() != ".csv")
            {
                strMessage = "Please fill only csv File for Resource_Text \n";
            }
            config = ConfigurationManager.AppSettings["Visio_CopyPath"];
            if (config == string.Empty)
            {
                strMessage += "Please fill Visio_CopyPath Path \n";
            }
            return strMessage;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {            
            Clear();
            new ConvertVisioFile().ShowDialog();
            EndResult();
        }
        
        private void EndResult()
        {
            txtJsonData.Text = clsReadVisioFile.strJsonData;
            if (clsReadVisioFile.strNotFoundComponentData == string.Empty)
            {
                clsReadVisioFile.strNotFoundComponentData = "No missing mappings";
            }
            txtNotFound.Text = clsReadVisioFile.strNotFoundComponentData.Replace("\r\n)", ")");
        }
        private string LoadXMLFilePath(string strPath)
        {
            if (strPath == getFilePath)
            {
                return System.Windows.Forms.Application.StartupPath + @"\" + clsConstant.CreateFolderName + @"\" + clsConstant.CreateXMLFile;
            }
            else
            {
                return System.Windows.Forms.Application.StartupPath + @"\" + clsConstant.CreateFolderName;
            }
        }

        private void BindList()
        {
            if(File.Exists(LoadXMLFilePath(getFilePath)))
            {
                ShowVisioFile();
                tvFile.Nodes.Clear();
                DataSet ds = new DataSet();

                ds.ReadXml(LoadXMLFilePath(getFilePath));

                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        TreeNode parentNode = new TreeNode();
                        parentNode = tvFile.Nodes.Add("File Name");
                        PopulateTreeView(parentNode, dt);
                        tvFile.ExpandAll();
                    }
                }
            }            
        }

        private void PopulateTreeView(TreeNode parentNode, DataTable dt)
        {
            foreach (DataRow dr in dt.Rows)
            {
                TreeNode childNode = new TreeNode();
                childNode.Text = dr["VisioFile"].ToString() + " " + dr["DateOfCreate"].ToString();
                childNode.Tag = dr["VisioFile"].ToString();
                childNode.ToolTipText = dr["VisioFile"].ToString();                
                parentNode.Nodes.Add(childNode);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtJsonData.Text != string.Empty)
            {
                SaveFile();
                Clear();
            }
            else
            {
                MessageBox.Show("Please Select Visio File");
            }
        }

        private void tvFile_DoubleClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            try
            {
                Clear();
                string currentPath = LoadXMLFilePath(getFolderPath);
                editRecord = tvFile.SelectedNode.Index;
                if (File.Exists(currentPath + @"\" + tvFile.SelectedNode.Tag.ToString().Trim()))
                {
                    ds = GetXMLData();
                    if (ds.Tables.Count > 0)
                    {
                        if(ds.Tables[0].Columns.Contains("WithoutConvertFile"))
                        {
                            clsReadVisioFile.isWithoutConvertFile = Convert.ToBoolean(ds.Tables[0].Rows[editRecord]["WithoutConvertFile"].ToString() == string.Empty ? "false" : ds.Tables[0].Rows[editRecord]["WithoutConvertFile"].ToString());
                        }
                        else
                        {
                            clsReadVisioFile.isWithoutConvertFile = false;
                        }
                    }
                    
                     new clsCreateThreatModel().CreateJson(currentPath, tvFile.SelectedNode.Tag.ToString().Trim());
                }
                else
                {
                    MessageBox.Show("There is no File exist", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                EndResult();
            }
            catch
            {

            }
        }


        public DataSet GetXMLData()
        {
            DataSet ds = new DataSet();
            if (File.Exists(LoadXMLFilePath(getFilePath)))
            {                              
                ds.ReadXml(LoadXMLFilePath(getFilePath));                               
            }
            return ds;
        }

        public void SaveFile()
        {
            SaveFileDialog saveFileDialog = null;
            StreamWriter streamWriter = null;
            try
            {
                if (txtJsonData.Text != string.Empty)
                {
                    saveFileDialog = new SaveFileDialog();
                    saveFileDialog.Filter = "Text File|*.txt";
                    saveFileDialog.Title = "Save Json File";
                    if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        SaveVisioFile();
                        string path = saveFileDialog.FileName;
                        streamWriter = new StreamWriter(File.Create(path));
                        streamWriter.Write(txtJsonData.Text);
                        streamWriter.Dispose();
                        MessageBox.Show("File has been Saved.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                saveFileDialog = null;
                streamWriter = null;
            }
        }

        public void SaveVisioFile()
        {
            string currentPath = string.Empty;
            string strFile = string.Empty;        
            XmlDocument xmlVisioFileDoc = null;
            DataSet ds = null;
            try
            {
                currentPath = LoadXMLFilePath(getFolderPath);

                if (!Directory.Exists(currentPath))
                {
                    Directory.CreateDirectory(currentPath);
                }

                strFile = LoadXMLFilePath(getFilePath);

                if (!(File.Exists(strFile)))
                {
                    xmlVisioFileDoc = new XmlDocument();
                    XmlElement ParentElement = xmlVisioFileDoc.CreateElement("FileRecords");
                    xmlVisioFileDoc.AppendChild(ParentElement);
                    xmlVisioFileDoc.Save(strFile);
                }

                File.Copy(clsReadVisioFile.strGetFilePath, ConfigurationManager.AppSettings["Visio_CopyPath"] + @"\" + clsReadVisioFile.strGetFileName, true);

                if (editRecord != 0)
                {
                    ds = new DataSet();
                    ds.ReadXml(strFile);
                    ds.Tables[0].Rows[editRecord]["VisioFile"] = clsReadVisioFile.strGetFileName;
                    ds.Tables[0].Rows[editRecord]["DateOfCreate"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                    ds.Tables[0].Rows[editRecord]["WithoutConvertFile"] = clsReadVisioFile.isWithoutConvertFile.ToString();
                    ds.WriteXml(strFile);
                }
                else
                {
                    xmlVisioFileDoc = new XmlDocument();

                    xmlVisioFileDoc.Load(strFile);
                    XmlElement ParentElement = xmlVisioFileDoc.CreateElement("File");
                    XmlElement FileName = xmlVisioFileDoc.CreateElement("VisioFile");
                    FileName.InnerText = clsReadVisioFile.strGetFileName;
                    XmlElement CreateDate = xmlVisioFileDoc.CreateElement("DateOfCreate");
                    CreateDate.InnerText = DateTime.Now.ToString("dd/MM/yyyy HH:mm");

                    XmlElement WithoutConvertFile = xmlVisioFileDoc.CreateElement("WithoutConvertFile");
                    WithoutConvertFile.InnerText = clsReadVisioFile.isWithoutConvertFile.ToString();                                     

                    ParentElement.AppendChild(FileName);
                    ParentElement.AppendChild(CreateDate);
                    ParentElement.AppendChild(WithoutConvertFile);
                    xmlVisioFileDoc.DocumentElement.AppendChild(ParentElement);
                    xmlVisioFileDoc.Save(strFile);
                }
                BindList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }           
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            editRecord = 0;
            txtJsonData.Text = string.Empty;
            clsReadVisioFile.strJsonData = string.Empty;
            txtNotFound.Text = string.Empty;
            clsReadVisioFile.strNotFoundComponentData = string.Empty;
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            Point p = new Point(10, 5);
            var data = RotatePoint(90, p);
        }

        public Point RotatePoint(float angle, Point pt)
        {
            var a = angle * System.Math.PI / 180.0;
            float cosa = (float)System.Math.Cos(a);
            float sina = (float)System.Math.Sin(a);
            int x = Convert.ToInt32(pt.X * cosa - pt.Y * sina);
            int y = Convert.ToInt32(pt.X * sina + pt.Y * cosa);
            return new Point(x, y);
        }


        private void ShowVisioFile()
        {
            DataSet ds = new DataSet();
            try
            {                
                ds.ReadXml(LoadXMLFilePath(getFilePath));
                int count = ds.Tables.Count> 0 ?  ds.Tables[0].Rows.Count - 1 : -1 ;
                for (int i = 0; i <= count; i++)
                {
                    if (!File.Exists(LoadXMLFilePath(getFolderPath) + @"\" + ds.Tables[0].Rows[i]["VisioFile"].ToString()))
                    {
                        ds.Tables[0].Rows.RemoveAt(i);
                        count = ds.Tables[0].Rows.Count - 1;
                        i = -1;
                    }
                }
                ds.WriteXml(LoadXMLFilePath(getFilePath));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                ds = null;
            }
        }
       
    }
}
