﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Packaging;
using Newtonsoft.Json;
using System.Threading;
using System.Xml.Serialization;
using Microsoft.Office.Interop.Visio;
using Aspose.Diagram;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;
using VisioLibrary;

namespace Visio
{
    public partial class ConvertVisioFile : Form
    {
        public ConvertVisioFile()
        {
            InitializeComponent();
        }

        OpenFileDialog ofd = new OpenFileDialog();
        bool isSelectFile = false;
        private void btnBrowser_Click(object sender, EventArgs e)
        {
            ofd.Filter = "Visio (*.vsdx;*.vsd;*.vss;*.vst;*.vdx)|*.vsdx;*.vsd;*.vss;*.vst;*.vdx";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                isSelectFile = true;
                txtFileName.Text = System.IO.Path.GetFullPath(ofd.FileName);
                btnTransfer.Enabled = true;
            }
            else
            {
                isSelectFile = false;
            }
        }

        private void btnTransfer_Click(object sender, EventArgs e)
        {
            string jsonText = string.Empty;
            string fileName = string.Empty;
            string directoryName = string.Empty;
            string fileExtension = string.Empty;                      
            DataSet ds = new DataSet();           
            try
            {
                if (isSelectFile)
                {
                    directoryName = System.IO.Path.GetDirectoryName(ofd.FileName);
                    fileName = System.IO.Path.GetFileName(ofd.FileName);
                    clsReadVisioFile.strGetFileName = fileName;
                    clsReadVisioFile.strGetFilePath = System.IO.Path.GetFullPath(ofd.FileName);
                    fileExtension = System.IO.Path.GetExtension(ofd.FileName);
                    clsReadVisioFile.isWithoutConvertFile = chkWithoutConvert.Checked;
                    new clsCreateThreatModel().CreateJson(directoryName, fileName);                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {               
                ds = null;               
                this.Close();
            }
        }
    }
}
