﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visio
{
    public class ThreatModelerModel
    {
        public List<NodeDataArray> nodeDataArray = new List<NodeDataArray>();
        public List<LinkDataArray> linkDataArray = new List<LinkDataArray>();

    }

    #region NodeDataArray
    public class NodeDataArray
    {
        public int Id { get; set; }
        public int key { get; set; }
        public string category { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string ImageURI { get; set; }
        public int NodeId { get; set; }
        public int Group { get; set; }
        public string Location { get; set; }
        public string BackgroundColor { get; set; }
        public string BorderColor { get; set; }
        public string TitleColor { get; set; }
        public string TitleBackgroundColor { get; set; }
        public bool isGroup { get; set; }
        public string isGraphExpanded { get; set; }
        public string FullName { get; set; }
        public string cpeid { get; set; }
        public int networkComponentId { get; set; }
        public decimal BorderThickness { get; set; }

        //Exta Property
        public string ForeColor { get; set; }
    }
    #endregion
    
    #region LinkDataArray
    public class LinkDataArray
    {
        public string Name { get; set; }
        public string Color { get; set; }       
        public int from { get; set; }
        public int to { get; set; }
        public bool IsVisible { get; set; }
        public int corner { get; set; }

        public string connecter { get; set; }
    }    
    #endregion    
   
}
