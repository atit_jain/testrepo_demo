﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.IO.Packaging;
using Newtonsoft.Json;
using System.Threading;
using System.Xml.Serialization;
using Microsoft.Office.Interop.Visio;
using Aspose.Diagram;
namespace Visio
{
    public partial class frmVisioToJson : Form
    {
        public frmVisioToJson()
        {
            InitializeComponent();
        }

        string strSaveFileMessage = "Save File";
       // private static readonly object ExceptionLogger;

        private void frmVisioToJson_Load(object sender, EventArgs e)
        {           
        }
       
        private void btnConvert_Click(object sender, EventArgs e)
        {            
            string strXml = string.Empty;
            string jsonText = string.Empty;
            string fileName = string.Empty;
            string directoryName = string.Empty;
            string fileExtension = string.Empty;
            XmlDocument doc = new XmlDocument();
            string dummyFileName = "drawing56823.vsdx";
            bool isOtherFile = false;
            try
            {                                            
                OpenFileDialog ofd = new OpenFileDialog();                
                ofd.Filter = "Visio (*.vsdx;*.vsd;*.vss;*.vst)|*.vsdx;*.vsd;*.vss;*.vst"; 
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    directoryName = System.IO.Path.GetDirectoryName(ofd.FileName);//openFileDialog1.InitialDirectory;
                    fileName = System.IO.Path.GetFileName(ofd.FileName);
                    fileExtension = System.IO.Path.GetExtension(ofd.FileName);

                    if(fileExtension.ToLower() == ".vsd" || fileExtension.ToLower() == ".vss"|| fileExtension.ToLower() == ".vst")
                    {
                        string _filename = fileName;
                        var diagram = new Aspose.Diagram.Diagram(directoryName + @"\" + _filename);
                        fileName = dummyFileName;
                        diagram.Save(directoryName + @"\" + fileName, SaveFileFormat.VSDX);
                        isOtherFile = true;
                    }

                                   
                    using (Package visioPackage = OpenPackage(fileName, directoryName))
                    {
                        strXml = "<start>";
                        IteratePackageParts(visioPackage);
                        PackagePart documentPart = GetPackagePart(visioPackage, "http://schemas.microsoft.com/visio/2010/relationships/document");

                        PackagePart pagesPart = GetPackagePart(visioPackage, documentPart, "http://schemas.microsoft.com/visio/2010/relationships/pages");
                        PackagePart pagePart = GetPackagePart(visioPackage, pagesPart, "http://schemas.microsoft.com/visio/2010/relationships/page");
                        XDocument pageXML = GetXMLFromPart(pagePart);

                        //IEnumerable<XElement> shapesXML1 = GetXElementsByName(pageXML, "Connects");

                        IEnumerable<XElement> shapesXML = GetXElementsByName(pageXML, "Shape");

                        var getShapeList = shapesXML.ToList();

                        for (int i = 0; i <= getShapeList.Count - 1; i++)
                        {
                            var xml = getShapeList[i];
                            strXml += xml.ToString();
                        }

                        strXml += "</start>";
                        doc.LoadXml(strXml);
                        jsonText = JsonConvert.SerializeXmlNode(doc);

                        SaveFileDialog sfd = new SaveFileDialog();
                        sfd.Filter = "Text File|*.txt";
                        sfd.Title = "Save Json File";
                        if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            string path = sfd.FileName;
                            StreamWriter sw = new StreamWriter(File.Create(path));
                            sw.Write(jsonText);
                            sw.Dispose();
                        }
                        txtJson.Text = jsonText;
                        MessageBox.Show(strSaveFileMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                if(isOtherFile)
                {
                    if (File.Exists(directoryName + @"\" + fileName))
                    {
                        File.Delete(directoryName + @"\" + fileName);
                    }
                }
            }
        }       

        private static Package OpenPackage(string fileName, string folder)
        {
            Package visioPackage = null;

            // Get a reference to the location 
            // where the Visio file is stored.
            //string directoryPath = System.Environment.GetFolderPath(
            //    folder);
            string directoryPath = folder;
            DirectoryInfo dirInfo = new DirectoryInfo(directoryPath);

            // Get the Visio file from the location.
            FileInfo[] fileInfos = dirInfo.GetFiles(fileName);
            if (fileInfos.Count() > 0)
            {
                FileInfo fileInfo = fileInfos[0];
                string filePathName = fileInfo.FullName;

                // Open the Visio file as a package with
                // read/write file access.
                visioPackage = Package.Open(
                    filePathName,
                    FileMode.Open,
                    FileAccess.ReadWrite);
            }

            // Return the Visio file as a package.
            return visioPackage;
        }
        private static void IteratePackageParts(Package filePackage)
        {
            string Uri = "";
            string ContentType = "";
            // Get all of the package parts contained in the package
            // and then write the URI and content type of each one to the console.
            PackagePartCollection packageParts = filePackage.GetParts();
            foreach (PackagePart part in packageParts)
            {
                //Console.WriteLine("Package part URI: {0}", part.Uri);
                //Console.WriteLine("Content type: {0}", part.ContentType.ToString());
                Uri += part.Uri + "\n";
                ContentType += part.ContentType.ToString() + "\n";


            }
        }

        private static PackagePart GetPackagePart(Package filePackage, string relationship)
        {

            // Use the namespace that describes the relationship 
            // to get the relationship.
            PackageRelationship packageRel =
                filePackage.GetRelationshipsByType(relationship).FirstOrDefault();
            PackagePart part = null;

            // If the Visio file package contains this type of relationship with 
            // one of its parts, return that part.
            if (packageRel != null)
            {

                // Clean up the URI using a helper class and then get the part.
                Uri docUri = PackUriHelper.ResolvePartUri(
                    new Uri("/", UriKind.Relative), packageRel.TargetUri);
                part = filePackage.GetPart(docUri);
            }
            return part;
        }

        private static PackagePart GetPackagePart(Package filePackage, PackagePart sourcePart, string relationship)
        {
            // This gets only the first PackagePart that shares the relationship
            // with the PackagePart passed in as an argument. You can modify the code
            // here to return a different PackageRelationship from the collection.
            PackageRelationship packageRel =
                sourcePart.GetRelationshipsByType(relationship).FirstOrDefault();
            PackagePart relatedPart = null;

            if (packageRel != null)
            {

                // Use the PackUriHelper class to determine the URI of PackagePart
                // that has the specified relationship to the PackagePart passed in
                // as an argument.
                Uri partUri = PackUriHelper.ResolvePartUri(
                    sourcePart.Uri, packageRel.TargetUri);
                relatedPart = filePackage.GetPart(partUri);
            }
            return relatedPart;
        }
        private static XDocument GetXMLFromPart(PackagePart packagePart)
        {
            XDocument partXml = null;

            // Open the packagePart as a stream and then 
            // open the stream in an XDocument object.
            Stream partStream = packagePart.GetStream();
            partXml = XDocument.Load(partStream);
            return partXml;
        }

        private static IEnumerable<XElement> GetXElementsByName(XDocument packagePart, string elementType)
        {
            // Construct a LINQ query that selects elements by their element type.
            IEnumerable<XElement> elements =
                from element in packagePart.Descendants()
                where element.Name.LocalName == elementType
                select element;

            // Return the selected elements to the calling code.
            return elements.DefaultIfEmpty(null);
        }

      
        //var diagram = new Aspose.Diagram.Diagram(@"C:\Users\Admin\Desktop\visio Doc\vst\Drawing1.vst");                
        //diagram.Save(@"C:\Users\Admin\Desktop\visio Doc\vst\Drawing1.vsdx",SaveFileFormat.VSDX);      

    }
}
