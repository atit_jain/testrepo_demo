﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aspose.Diagram;
using System.Windows.Forms;
using System.Diagnostics;

namespace Visio.Class
{
    public class clsCreateThreatModel
    {
        public void CreateJson(string directoryName, string fileName)
        {
            string oldFile = fileName;
            string oldDirectoryName = directoryName;
            string dummyFileName = "drawing56823.vsdx";
            string fileExtension = string.Empty;
            bool isOtherFile = false;
            string jsonText = string.Empty;
            DataSet ds = new DataSet();
            ThreatModelerModel threadModel = null;
            try
            {
                clsReadVisioFile.strNotFoundComponentData = string.Empty;
                fileExtension = System.IO.Path.GetExtension(fileName);
                if (!clsReadVisioFile.isWithoutConvertFile)
                {
                    if (fileExtension.ToLower() == clsConstant.Extension_vsdx || fileExtension.ToLower() == clsConstant.Extension_vsd || fileExtension.ToLower() == clsConstant.Extension_vss || fileExtension.ToLower() == clsConstant.Extension_vst)
                    {
                        if (File.Exists(directoryName + @"\" + dummyFileName))
                        {
                            File.Delete(directoryName + @"\" + dummyFileName);
                        }
                        string _filename = fileName;

                        var diagram = new Aspose.Diagram.Diagram(directoryName + @"\" + _filename);
                        fileName = dummyFileName;
                        diagram.Save(directoryName + @"\" + fileName, SaveFileFormat.VSDX);
                        isOtherFile = true;
                    }
                }

                ds = new clsReadVisioFile().CreateDataSetFromVisio(fileName, directoryName);

                threadModel = new clsCreateThreatModel().CreateThreatModel(ds);
                if (threadModel.nodeDataArray.Count == 0 || threadModel.linkDataArray.Count == 0)
                {
                    if (!clsReadVisioFile.isWithoutConvertFile)
                    {
                        if (isOtherFile)
                        {
                            if (File.Exists(directoryName + @"\" + fileName))
                            {
                                File.Delete(directoryName + @"\" + fileName);
                            }
                        }
                        clsReadVisioFile.isWithoutConvertFile = true;
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        CreateJson(oldDirectoryName, oldFile);
                        return;
                    }
                }
                jsonText = Newtonsoft.Json.JsonConvert.SerializeObject(threadModel);
                clsReadVisioFile.strJsonData = jsonText;
            }
            catch (Exception ex)
            {
                if (ex.Message == "A column named 'Text' already belongs to this DataTable: cannot set a nested table name to the same name.")
                {
                    MessageBox.Show("Please again save as this File in vsdx Format");
                }
                else
                {
                    throw ex;
                }

            }
            finally
            {
                if (isOtherFile)
                {
                    if (File.Exists(directoryName + @"\" + fileName))
                    {
                        File.Delete(directoryName + @"\" + fileName);
                    }
                }
            }
        }

        internal Dictionary<string, string> GetDict(DataTable dt)
        {
            return dt.AsEnumerable()
              .ToDictionary<DataRow, string, string>(row => row.Field<string>(0),
                                        row => row.Field<string>(1));
        }

        public ThreatModelerModel CreateThreatModel(DataSet ds)
        {
            ThreatModelerModel threadModel = new ThreatModelerModel();
            KeyValuePair<string, string> dataResource = new KeyValuePair<string, string>();
            KeyValuePair<string, string> dataResource_Text = new KeyValuePair<string, string>();
            EnumerableRowCollection<DataRow> getConnect = null;
            DataView viewConnect = null;
            DataRow getDataRow = null;
            bool isAddLink = false;
            List<NodeDataArray> nodeDataArray = new List<NodeDataArray>();
            List<LinkDataArray> linkDataArray = new List<LinkDataArray>();

            try
            {                
                string[] s = { "\\bin" };
                var dataResourceFile = GetDict(GetDataTabletFromCSVFile(Application.StartupPath.Split(s, StringSplitOptions.None)[0] + "\\Resources\\ResourceCSV\\Resource_Mapping.csv"));
                var dataResourceFile_Text = GetDict(GetDataTabletFromCSVFile(Application.StartupPath.Split(s, StringSplitOptions.None)[0] + "\\Resources\\ResourceCSV\\Resource_Text.csv"));

               // var dataResourceFile = new clsReadResourceFile().ReadResourceFile("Visio.Resources.Resource_Mapping");
               // var dataResourceFile_Text = new clsReadResourceFile().ReadResourceFile("Visio.Resources.Resource_Text");
                ds = SetAllNameByMaster(ds);
                for (int i = 0; i <= ds.Tables["Shape"].Rows.Count - 1; i++)
                {
                    if (ds.Tables["Shape"].Rows[i]["Name"].ToString() != string.Empty || ds.Tables["Shape"].Rows[i]["TextName"].ToString() != string.Empty)
                    {
                        if (ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() != clsConstant.DynamicConnector &&
                           ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() != clsConstant.OneWayDataConnection &&
                           ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() != clsConstant.TwoWayDataConnection)
                        {
                            string aa = ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower();
                            dataResource_Text = new KeyValuePair<string, string>();
                            dataResource = new KeyValuePair<string, string>();
                            if (ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() == clsConstant_Shape.Foreign)
                            {
                                dataResource_Text = dataResourceFile_Text.Where(x => x.Key.ToLower() == ds.Tables["Shape"].Rows[i]["TextName"].ToString().Replace(" ", string.Empty).Trim().ToLower())
                                                .FirstOrDefault();
                                if (dataResource_Text.Key == null)
                                {
                                    dataResource = dataResourceFile.Where(x => x.Key.ToLower() == ds.Tables["Shape"].Rows[i]["TextName"].ToString().Replace(" ", string.Empty).Trim().ToLower())
                                                .FirstOrDefault();
                                }
                            }
                            else
                            {
                                dataResource_Text = dataResourceFile_Text.Where(x => x.Key.ToLower() == ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower()
                                   || x.Key.ToLower() == ds.Tables["Shape"].Rows[i]["TextName"].ToString().Replace(" ", string.Empty).Trim().ToLower()).FirstOrDefault();
                                if (dataResource_Text.Key == null)
                                {
                                    dataResource = dataResourceFile.Where(x => x.Key.ToLower() == ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower()
                                   || x.Key.ToLower() == ds.Tables["Shape"].Rows[i]["TextName"].ToString().Replace(" ", string.Empty).Trim().ToLower()).FirstOrDefault();
                                }
                            }

                            if (dataResource.Key != null || dataResource_Text.Key != null)
                            {
                                string fullName = ds.Tables["Shape"].Rows[i]["TextName"].ToString();
                                ds.Tables["Shape"].Rows[i]["ThreadShape"] = true;
                                nodeDataArray.Add(ReturnNodeDataArray(ds, i, dataResource, dataResource_Text, fullName));
                            }
                            else
                            {
                                ds.Tables["Shape"].Rows[i]["ThreadShape"] = false;
                                if (clsReadVisioFile.strNotFoundComponentData == string.Empty)
                                {
                                    clsReadVisioFile.strNotFoundComponentData = "Component Name (Text Value)";
                                    clsReadVisioFile.strNotFoundComponentData += Environment.NewLine + ds.Tables["Shape"].Rows[i]["Name"].ToString() + " (" + ds.Tables["Shape"].Rows[i]["TextName"].ToString() + ")";
                                }
                                else
                                {
                                    clsReadVisioFile.strNotFoundComponentData += Environment.NewLine + ds.Tables["Shape"].Rows[i]["Name"].ToString() + " (" + ds.Tables["Shape"].Rows[i]["TextName"].ToString() + ")";
                                }
                            }
                        }
                        //else
                        //{
                        //isAddLink = false;
                        //getConnect = ds.Tables["Connect"].AsEnumerable().Where(row => row.Field<string>
                        //      ("FromSheet") == ds.Tables["Shape"].Rows[i]["ID"].ToString()).Select(x => x);
                        //viewConnect = getConnect.AsDataView();
                        //foreach (DataRowView drv in viewConnect)
                        //{
                        //    getDataRow = ds.Tables["Shape"].AsEnumerable().Where(row => row.Field<string>
                        //                        ("ID") == drv["ToSheet"].ToString()).Select(x => x).FirstOrDefault();
                        //    isAddLink = Convert.ToBoolean(getDataRow["ThreadShape"].ToString() == string.Empty ? false: getDataRow["ThreadShape"]) ? true : false;
                        //    if (isAddLink == false)
                        //        break;
                        //}
                        //if (isAddLink == true)
                        //    threadModel.linkDataArray.Add(ReturnLinkDataArray(ds, i));
                        //}
                    }
                }

                for (int i = 0; i <= ds.Tables["Shape"].Rows.Count - 1; i++)
                {
                    if (ds.Tables["Shape"].Rows[i]["Name"].ToString() != string.Empty)
                    {
                        if (ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() == clsConstant.DynamicConnector ||
                           ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() == clsConstant.OneWayDataConnection ||
                           ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() == clsConstant.TwoWayDataConnection)
                        {
                            isAddLink = false;
                            getConnect = ds.Tables["Connect"].AsEnumerable().Where(row => row.Field<string>
                                  ("FromSheet") == ds.Tables["Shape"].Rows[i]["ID"].ToString()).Select(x => x);

                            viewConnect = getConnect.AsDataView();
                            foreach (DataRowView drv in viewConnect)
                            {
                                getDataRow = ds.Tables["Shape"].AsEnumerable().Where(row => row.Field<string>
                                                    ("ID") == drv["ToSheet"].ToString()).Select(x => x).FirstOrDefault();

                                isAddLink = Convert.ToBoolean(getDataRow["ThreadShape"].ToString() == string.Empty ? false : getDataRow["ThreadShape"]) ? true : false;
                                if (isAddLink == false)
                                    break;
                            }

                            if (isAddLink == true)
                                linkDataArray.Add(ReturnLinkDataArray(ds, i));
                        }
                    }
                }
                threadModel.nodeDataArray = nodeDataArray;
                threadModel.linkDataArray = linkDataArray;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                getConnect = null;
                viewConnect = null;
                getDataRow = null;
            }
            return threadModel;
        }


        public static DataTable GetDataTabletFromCSVFile(string csv_file_path)
        {
            DataTable csvData = new DataTable();
            try
            {
                using (Microsoft.VisualBasic.FileIO.TextFieldParser csvReader = new Microsoft.VisualBasic.FileIO.TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    //read column names
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        System.Data.DataColumn datecolumn = new System.Data.DataColumn(column);
                        datecolumn.AllowDBNull = true;
                        csvData.Columns.Add(datecolumn);
                    }
                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        //Making empty value as null
                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = null;
                            }
                        }
                        csvData.Rows.Add(fieldData);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return csvData;
        }






        private DataSet SetAllNameByMaster(DataSet ds)
        {
            //do not remve this loop because some time name not fount in first shape
            for (int i = 0; i <= ds.Tables["Shape"].Rows.Count - 1; i++)
            {
                if (ds.Tables["Shape"].Rows[i]["Type"].ToString().ToLower() == clsConstant_Shape.Foreign)
                {
                    ds.Tables["Shape"].Rows[i]["Name"] = clsConstant_Shape.Foreign;
                    ds.Tables["Shape"].Rows[i]["NameU"] = clsConstant_Shape.Foreign;
                    ds.Tables["Shape"].Rows[i]["Master"] = "5682";
                }

                if (ds.Tables["Shape"].Rows[i]["NameU"].ToString() != string.Empty)
                {
                    ds.Tables["Shape"].AsEnumerable().Where(row => row.Field<string>("Master") == ds.Tables["Shape"].Rows[i]["Master"].ToString())
                    .Select(b => b["Name"] = ds.Tables["Shape"].Rows[i]["Name"].ToString().Replace(" ", string.Empty))
                    .ToList();
                }
            }
            return ds;
        }

        private NodeDataArray ReturnNodeDataArray(DataSet ds, int index, KeyValuePair<string, string> dataResource, KeyValuePair<string, string> dataResource_Text, string fullName)
        {
            NodeDataArray nodeDataArray = new NodeDataArray();
            EnumerableRowCollection<DataRow> getCell = null;
            DataView viewCell = null;
            try
            {
                if (dataResource.Key == clsConstant_Shape.Foreign)
                {
                    nodeDataArray.ImageURI = dataResource.Value;
                    nodeDataArray.FullName = fullName;//ds.Tables["Shape"].Rows[index]["TextName"].ToString();
                }
                else
                {
                    //if(fullName != string.Empty)
                    //{
                    //    nodeDataArray.FullName = fullName;
                    //}
                    //else
                    //{
                    //    nodeDataArray.FullName = dataResource_Text.Key != null ? dataResource_Text.Value : dataResource.Value;
                    //}

                    nodeDataArray.FullName = fullName;
                    nodeDataArray.Name = dataResource_Text.Key != null ? dataResource_Text.Value : dataResource.Value;
                }

                nodeDataArray.Id = Convert.ToInt32(ds.Tables["Shape"].Rows[index]["ID"].ToString());
                // nodeDataArray.Name = ds.Tables["Shape"].Rows[index]["Type"].ToString();
                nodeDataArray.Type = "Visio Shape: " + ds.Tables["Shape"].Rows[index]["Name"].ToString(); //ds.Tables["Shape"].Rows[index]["Type"].ToString();
                nodeDataArray.category = ds.Tables["Shape"].Rows[index]["Type"].ToString();

                getCell = ds.Tables["Cell"].AsEnumerable().Where(row => row.Field<int?>
                                      ("Shape_Id") == Convert.ToInt32(ds.Tables["Shape"].Rows[index]["Shape_id"].ToString())).Select(x => x);

                viewCell = getCell.AsDataView();

                foreach (DataRowView drv in viewCell)
                {
                    if (drv["N"].ToString().ToLower() == clsConstant.PinX)
                    {
                        nodeDataArray.Location = drv["V"].ToString() + " ";
                    }
                    else if (drv["N"].ToString().ToLower() == clsConstant.PinY)
                    {
                        nodeDataArray.Location += drv["V"].ToString();
                    }
                    else if (drv["N"].ToString().ToLower() == clsConstant.FillForegnd)
                    {
                        nodeDataArray.ForeColor += drv["V"].ToString();
                    }
                    else if (drv["N"].ToString().ToLower() == clsConstant.FillForegnd)
                    {
                        nodeDataArray.TitleColor += drv["V"].ToString();
                    }
                    else if (drv["N"].ToString().ToLower() == clsConstant.Fillbkgnd)
                    {
                        nodeDataArray.BackgroundColor += drv["V"].ToString();
                    }
                    else if (drv["N"].ToString().ToLower() == clsConstant.LineColor)
                    {
                        nodeDataArray.BorderColor += drv["V"].ToString();
                    }
                    else if (drv["N"].ToString().ToLower() == clsConstant.LineWeight)
                    {
                        nodeDataArray.BorderThickness = Convert.ToDecimal(drv["V"].ToString());
                    }
                    else if (drv["N"].ToString().ToLower() == clsConstant.Relationships)
                    {
                        if (ds.Tables["Shape"].Rows[index]["Name"].ToString().ToLower() != clsConstant_Shape.Plain)
                        {
                            if (drv["F"].ToString().Contains("SUM(DEPENDSON(4,Sheet."))
                            {
                                nodeDataArray.Group = Convert.ToInt32(drv["F"].ToString().Replace("SUM(DEPENDSON(4,Sheet.", "").Replace("!SheetRef()))", ""));
                            }
                        }
                        else
                        {
                            nodeDataArray.isGroup = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                getCell = null;
                viewCell = null;
            }
            return nodeDataArray;
        }

        private LinkDataArray ReturnLinkDataArray(DataSet ds, int index)
        {
            LinkDataArray linkDataArray = new LinkDataArray();
            EnumerableRowCollection<DataRow> getCell = null;
            EnumerableRowCollection<DataRow> getConnect = null;
            DataView viewConnect = null;
            DataView viewCell = null;
            bool isBeginArrow = false;
            bool isTwoWayDirection = false;
            try
            {
                linkDataArray.Name = ds.Tables["Shape"].Rows[index]["TextName"].ToString().ToLower();
                //linkDataArray.connecter = Connector(ds.Tables["Shape"].Rows[index]["Name"].ToString().ToLower());
                linkDataArray.IsVisible = true;

                getCell = ds.Tables["Cell"].AsEnumerable().Where(row => row.Field<int?>
                                      ("Shape_Id") == Convert.ToInt32(ds.Tables["Shape"].Rows[index]["Shape_id"].ToString())).Select(x => x);

                viewCell = getCell.AsDataView();
                foreach (DataRowView drv in viewCell)
                {
                    if (drv["N"].ToString().ToLower() == clsConstant.LineColor)
                    {
                        linkDataArray.Color = drv["V"].ToString();
                    }

                    if (ds.Tables["Shape"].Rows[index]["Name"].ToString().ToLower() == clsConstant.TwoWayDataConnection.ToLower())
                    {
                        isTwoWayDirection = true;
                    }
                    else
                    {
                        if (drv["N"].ToString().ToLower() == clsConstant.BeginArrow || drv["N"].ToString().ToLower() == clsConstant.EndArrow)
                        {

                            if (drv["V"].ToString().ToLower() != string.Empty)
                            {
                                if (drv["N"].ToString().ToLower() == clsConstant.BeginArrow)
                                {
                                    isTwoWayDirection = Convert.ToInt32(drv["V"].ToString()) != 0 ? true : false;
                                    isBeginArrow = true;
                                }
                                else
                                {
                                    if (isBeginArrow)
                                    {
                                        isTwoWayDirection = Convert.ToInt32(drv["V"].ToString()) != 0 ? true : false;
                                    }
                                }

                            }
                        }
                    }
                }

                if (isTwoWayDirection)
                {
                    linkDataArray.connecter = "Bi directional";
                }
                else
                {
                    linkDataArray.connecter = "unidirectional";
                }
                getConnect = ds.Tables["Connect"].AsEnumerable().Where(row => row.Field<string>
                                      ("FromSheet") == ds.Tables["Shape"].Rows[index]["ID"].ToString()).Select(x => x);

                viewConnect = getConnect.AsDataView();

                foreach (DataRowView drv in viewConnect)
                {
                    if (drv["FromCell"].ToString().ToLower() == clsConstant.Beginx)
                    {
                        linkDataArray.from = Convert.ToInt32(drv["ToSheet"].ToString());
                    }
                    else if (drv["FromCell"].ToString().ToLower() == clsConstant.Endx)
                    {
                        linkDataArray.to = Convert.ToInt32(drv["ToSheet"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                getCell = null;
                getConnect = null;
                viewConnect = null;
                viewCell = null;
            }
            return linkDataArray;
        }

        //private string Connector(string Name)
        //{
        //    string connectorName = string.Empty;
        //    if(clsConstant.TwoWayDataConnection.ToLower() == Name.ToLower())
        //    {
        //        connectorName = "Bi directional";
        //    }
        //    else
        //    {
        //        connectorName = "unidirectional";
        //    }
        //    return connectorName;
        //}
    }
}
