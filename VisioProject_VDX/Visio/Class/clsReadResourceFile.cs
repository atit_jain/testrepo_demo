﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Resources;
using System.Globalization;
using System.Collections;

namespace Visio.Class
{
    interface IReadResourceFile
    {
       Dictionary<string, string> ReadResourceFile(string strResource);          
    }

    public class clsReadResourceFile : IReadResourceFile
    {       
        public  Dictionary<string, string> ReadResourceFile(string strResource)
        {
            Dictionary<string, string> dicResource = new Dictionary<string, string>();          
            ResourceSet resourceSet = null;
            try
            {
               
                resourceSet = new ResourceManager(strResource, Assembly.GetExecutingAssembly()).GetResourceSet(CultureInfo.CurrentUICulture, true, true);
                foreach (DictionaryEntry entry in resourceSet)
                {                    
                    dicResource.Add(entry.Key.ToString(), entry.Value.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {             
                resourceSet = null;
            }           
            return dicResource;
        }
    }
}
