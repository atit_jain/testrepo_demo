﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.IO.Packaging;
using Newtonsoft.Json;
using System.Threading;
using System.Xml.Serialization;
using Microsoft.Office.Interop.Visio;
using Aspose.Diagram;
using System.Data;
using Newtonsoft.Json.Linq;

namespace Visio.Class
{
    public class clsReadVisioFile
    {
        public static string strJsonData = string.Empty;
        public static string strNotFoundComponentData = string.Empty;
        public static string strGetFileName = string.Empty;
        public static string strGetFilePath = string.Empty;
        public static bool isWithoutConvertFile = false;
        public Package OpenPackage(string fileName, string folder)
        {
            Package visioPackage = null;

            // Get a reference to the location 
            // where the Visio file is stored.
            //string directoryPath = System.Environment.GetFolderPath(
            //    folder);
            string directoryPath = folder;
            DirectoryInfo dirInfo = new DirectoryInfo(directoryPath);

            // Get the Visio file from the location.
            FileInfo[] fileInfos = dirInfo.GetFiles(fileName);
            if (fileInfos.Count() > 0)
            {
                FileInfo fileInfo = fileInfos[0];
                string filePathName = fileInfo.FullName;

                // Open the Visio file as a package with
                // read/write file access.
                visioPackage = Package.Open(
                    filePathName,
                    FileMode.Open,
                    FileAccess.ReadWrite);
            }

            // Return the Visio file as a package.
            return visioPackage;
        }
        public void IteratePackageParts(Package filePackage)
        {
            string Uri = "";
            string ContentType = "";
            // Get all of the package parts contained in the package
            // and then write the URI and content type of each one to the console.
            PackagePartCollection packageParts = filePackage.GetParts();
            foreach (PackagePart part in packageParts)
            {
                //Console.WriteLine("Package part URI: {0}", part.Uri);
                //Console.WriteLine("Content type: {0}", part.ContentType.ToString());
                Uri += part.Uri + "\n";
                ContentType += part.ContentType.ToString() + "\n";


            }
        }

        public PackagePart GetPackagePart(Package filePackage, string relationship)
        {

            // Use the namespace that describes the relationship 
            // to get the relationship.
            PackageRelationship packageRel =
                filePackage.GetRelationshipsByType(relationship).FirstOrDefault();
            PackagePart part = null;

            // If the Visio file package contains this type of relationship with 
            // one of its parts, return that part.
            if (packageRel != null)
            {

                // Clean up the URI using a helper class and then get the part.
                Uri docUri = PackUriHelper.ResolvePartUri(
                    new Uri("/", UriKind.Relative), packageRel.TargetUri);
                part = filePackage.GetPart(docUri);
            }
            return part;
        }

        public PackagePart GetPackagePart(Package filePackage, PackagePart sourcePart, string relationship)
        {
            // This gets only the first PackagePart that shares the relationship
            // with the PackagePart passed in as an argument. You can modify the code
            // here to return a different PackageRelationship from the collection.
            PackageRelationship packageRel =
                sourcePart.GetRelationshipsByType(relationship).FirstOrDefault();
            PackagePart relatedPart = null;

            if (packageRel != null)
            {

                // Use the PackUriHelper class to determine the URI of PackagePart
                // that has the specified relationship to the PackagePart passed in
                // as an argument.
                Uri partUri = PackUriHelper.ResolvePartUri(
                    sourcePart.Uri, packageRel.TargetUri);
                relatedPart = filePackage.GetPart(partUri);
            }
            return relatedPart;
        }
        public XDocument GetXMLFromPart(PackagePart packagePart)
        {
            XDocument partXml = null;

            // Open the packagePart as a stream and then 
            // open the stream in an XDocument object.
            Stream partStream = packagePart.GetStream();
            partXml = XDocument.Load(partStream);
            return partXml;
        }

        public IEnumerable<XElement> GetXElementsByName(XDocument packagePart, string elementType)
        {
            // Construct a LINQ query that selects elements by their element type.
            IEnumerable<XElement> elements =
                from element in packagePart.Descendants()
                where element.Name.LocalName == elementType
                select element;

            // Return the selected elements to the calling code.
            return elements.DefaultIfEmpty(null);
        }

        public DataSet CreateDataSetFromVisio(string fileName, string directoryName)
        {
            DataSet ds = new DataSet();

            using (Package visioPackage = OpenPackage(fileName, directoryName))
            {
                IteratePackageParts(visioPackage);
                PackagePart documentPart = GetPackagePart(visioPackage, "http://schemas.microsoft.com/visio/2010/relationships/document");

                PackagePart pagesPart = GetPackagePart(visioPackage, documentPart, "http://schemas.microsoft.com/visio/2010/relationships/pages");
                PackagePart pagePart = GetPackagePart(visioPackage, pagesPart, "http://schemas.microsoft.com/visio/2010/relationships/page");
                XDocument pageXML = GetXMLFromPart(pagePart);

                XmlReader xmlReader = pageXML.CreateReader();
                ds.ReadXml(xmlReader);

                ds.Tables["Shape"].Columns.Add("TextName", typeof(string));
                ds.Tables["Shape"].Columns.Add("ThreadShape", typeof(bool));
                //IEnumerable<XElement> shapesXML1 = GetXElementsByName(pageXML, "Connects");

                for(int i=0;i<= ds.Tables["Shape"].Rows.Count-1;i++)
                {
                    if(ds.Tables["Shape"].Rows[i]["NameU"].ToString().Trim() != string.Empty)
                    {
                        string str = ds.Tables["Shape"].Rows[i]["NameU"].ToString();
                        if (ds.Tables["Shape"].Rows[i]["NameU"].ToString().Contains("."))
                        {
                            ds.Tables["Shape"].Rows[i]["NameU"] = str.Substring(0, str.IndexOf("."));
                        }
                    }
                    if (ds.Tables["Shape"].Rows[i]["Name"].ToString().Trim() != string.Empty)
                    {
                        string str = ds.Tables["Shape"].Rows[i]["Name"].ToString();
                        if (ds.Tables["Shape"].Rows[i]["Name"].ToString().Contains("."))
                        {
                            ds.Tables["Shape"].Rows[i]["Name"] = str.Substring(0, str.IndexOf("."));
                        }
                    }
                }                   

                IEnumerable<XElement> shapesXML = GetXElementsByName(pageXML, "Shape");
                var getShapeList = shapesXML.ToList();


                for (int i = 0; i <= getShapeList.Count - 1; i++)
                {
                    var xml = getShapeList[i];
                    var xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(xml.ToString());
                    string json = JsonConvert.SerializeXmlNode(xmlDocument);

                    JObject obj = JObject.Parse(json);
                    if (json.Contains("Text"))
                    {
                        string strID = (string)obj["Shape"]["@ID"];
                        string strText = string.Empty;
                        if (obj["Shape"]["Text"] != null)
                        {
                            if (json.Contains("#text"))//(json.Contains("#text"))
                            {
                                if ((obj["Shape"]["Text"]["#text"].Type.ToString()) != "Array")
                                {
                                    strText = (string)obj["Shape"]["Text"]["#text"];
                                }
                            }
                            else
                            {

                                if ((obj["Shape"]["Text"].Type.ToString()) != "Array" && (obj["Shape"]["Text"].Type.ToString()) != "Object")
                                {
                                    strText = (string)obj["Shape"]["Text"];
                                }
                            }
                        }
                        ds.Tables["Shape"].AsEnumerable().Where(row => row.Field<string>("ID") == strID)
                                .Select(b => b["TextName"] = strText).ToList();
                    }
                }
               
            }           
            return ds;
        }

    }
}
