﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visio.Class
{
    public  class clsConstant
    {
        public const string CreateFolderName = "xmlFile";
        public const string CreateXMLFile = "FileRecord.xml";
        public const string Extension_vsdx = ".vsdx";
        public const string Extension_vsd =  ".vsd";
        public const string Extension_vss =".vss";
        public const string Extension_vst = ".vst";
        public const string DynamicConnector = "dynamicconnector";
        public const string OneWayDataConnection = "1-waydataconnection";
        public const string TwoWayDataConnection = "2-waydataconnection";
        public const string PinX = "pinx";
        public const string PinY = "piny";
        public const string FillForegnd = "fillforegnd";
        public const string Fillbkgnd = "fillbkgnd";
        public const string LineColor = "linecolor";
        public const string LineWeight = "lineweight";
        public const string Relationships = "relationships";
        public const string Beginx = "beginx";
        public const string Endx = "endx";
        public const string BeginArrow = "beginarrow";
        public const string EndArrow = "endarrow";
    }

    public class clsConstant_Shape
    {
        public const string Plain = "plain";
        public const string Foreign = "foreign";
    }
}
