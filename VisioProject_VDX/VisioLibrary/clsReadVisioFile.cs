﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.IO.Packaging;
using Newtonsoft.Json;
using System.Threading;
using System.Xml.Serialization;
using Microsoft.Office.Interop.Visio;
using Aspose.Diagram;
using System.Data;
using Newtonsoft.Json.Linq;

using System.Drawing;
using System.Drawing.Drawing2D;

namespace VisioLibrary
{
    public class clsReadVisioFile
    {
        public static string strJsonData = string.Empty;
        public static string strNotFoundComponentData = string.Empty;
        public static string strGetFileName = string.Empty;
        public static string strGetFilePath = string.Empty;
        public static bool isWithoutConvertFile = false;
        public Package OpenPackage(string fileName, string folder)
        {
            Package visioPackage = null;

            // Get a reference to the location 
            // where the Visio file is stored.
            //string directoryPath = System.Environment.GetFolderPath(
            //    folder);
            string directoryPath = folder;
            DirectoryInfo dirInfo = new DirectoryInfo(directoryPath);

            // Get the Visio file from the location.
            FileInfo[] fileInfos = dirInfo.GetFiles(fileName);
            if (fileInfos.Count() > 0)
            {
                FileInfo fileInfo = fileInfos[0];
                string filePathName = fileInfo.FullName;

                // Open the Visio file as a package with
                // read/write file access.
                visioPackage = Package.Open(
                    filePathName,
                    FileMode.Open,
                    FileAccess.ReadWrite);
            }

            // Return the Visio file as a package.
            return visioPackage;
        }
        public void IteratePackageParts(Package filePackage)
        {
            string Uri = "";
            string ContentType = "";
            // Get all of the package parts contained in the package
            // and then write the URI and content type of each one to the console.
            PackagePartCollection packageParts = filePackage.GetParts();
            foreach (PackagePart part in packageParts)
            {
                //Console.WriteLine("Package part URI: {0}", part.Uri);
                //Console.WriteLine("Content type: {0}", part.ContentType.ToString());
                Uri += part.Uri + "\n";
                ContentType += part.ContentType.ToString() + "\n";


            }
        }

        public PackagePart GetPackagePart(Package filePackage, string relationship)
        {

            // Use the namespace that describes the relationship 
            // to get the relationship.
            PackageRelationship packageRel =
                filePackage.GetRelationshipsByType(relationship).FirstOrDefault();
            PackagePart part = null;

            // If the Visio file package contains this type of relationship with 
            // one of its parts, return that part.
            if (packageRel != null)
            {

                // Clean up the URI using a helper class and then get the part.
                Uri docUri = PackUriHelper.ResolvePartUri(
                    new Uri("/", UriKind.Relative), packageRel.TargetUri);
                part = filePackage.GetPart(docUri);
            }
            return part;
        }

        public PackagePart GetPackagePart(Package filePackage, PackagePart sourcePart, string relationship)
        {
            // This gets only the first PackagePart that shares the relationship
            // with the PackagePart passed in as an argument. You can modify the code
            // here to return a different PackageRelationship from the collection.
            PackageRelationship packageRel =
                sourcePart.GetRelationshipsByType(relationship).FirstOrDefault();
            PackagePart relatedPart = null;

            if (packageRel != null)
            {

                // Use the PackUriHelper class to determine the URI of PackagePart
                // that has the specified relationship to the PackagePart passed in
                // as an argument.
                Uri partUri = PackUriHelper.ResolvePartUri(
                    sourcePart.Uri, packageRel.TargetUri);
                relatedPart = filePackage.GetPart(partUri);
            }
            return relatedPart;
        }
        public XDocument GetXMLFromPart(PackagePart packagePart)
        {
            XDocument partXml = null;

            // Open the packagePart as a stream and then 
            // open the stream in an XDocument object.
            Stream partStream = packagePart.GetStream();
            partXml = XDocument.Load(partStream);
            return partXml;
        }

        public IEnumerable<XElement> GetXElementsByName(XDocument packagePart, string elementType)
        {
            // Construct a LINQ query that selects elements by their element type.
            IEnumerable<XElement> elements =
                from element in packagePart.Descendants()
                where element.Name.LocalName == elementType
                select element;

            // Return the selected elements to the calling code.
            return elements.DefaultIfEmpty(null);
        }

        public DataSet CreateDataSetFromVisio(string fileName, string directoryName, XMLModel xmlModel)
        {
            DataSet ds = new DataSet();
            DataTable dt_Coordinate = new DataTable();
            using (Package visioPackage = OpenPackage(fileName, directoryName))
            {
                IteratePackageParts(visioPackage);
                PackagePart documentPart = GetPackagePart(visioPackage, "http://schemas.microsoft.com/visio/2010/relationships/document");

                PackagePart pagesPart = GetPackagePart(visioPackage, documentPart, "http://schemas.microsoft.com/visio/2010/relationships/pages");
                PackagePart pagePart = GetPackagePart(visioPackage, pagesPart, "http://schemas.microsoft.com/visio/2010/relationships/page");
                XDocument pageXML = GetXMLFromPart(pagePart);

                XmlReader xmlReader = pageXML.CreateReader();
                ds.ReadXml(xmlReader);
                ds.Tables["Shape"].Columns.Add("IsContainer", typeof(bool));
                ds.Tables["Shape"].Columns.Add("ContainerShapeId", typeof(string));
                ds.Tables["Shape"].Columns.Add("TextName", typeof(string));
                ds.Tables["Shape"].Columns.Add("ThreadShape", typeof(bool));

                for (int i = 0; i <= ds.Tables["Shape"].Rows.Count - 1; i++)
                {
                    if (ds.Tables["Shape"].Rows[i]["Name"].ToString().Trim() != string.Empty)
                    {

                        string str = ds.Tables["Shape"].Rows[i]["Name"].ToString();
                        str = str.Replace("com.lucidchart.", "");
                        str = str.Replace("Block", ""); //For Basic Flowchart
                        str = str.Replace("2017", ""); //for AWS Shape Name
                        ds.Tables["Shape"].Rows[i]["Name"] = str;
                        if (ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower().Contains("container"))
                        {
                            ds.Tables["Shape"].Rows[i]["IsContainer"] = true;
                        }
                    }
                }

                //  dt_Coordinate = FindOut_Coordinate(ds, xmlModel);

                IEnumerable<XElement> shapesXML = GetXElementsByName(pageXML, "Shape");
                var getShapeList = shapesXML.ToList();

                for (int i = 0; i <= getShapeList.Count - 1; i++)
                {
                    var xml = getShapeList[i];
                    var xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(xml.ToString());
                    string json = JsonConvert.SerializeXmlNode(xmlDocument);

                    JObject obj = JObject.Parse(json);
                    if (json.Contains("Text"))
                    {
                        string strID = (string)obj["Shape"]["@ID"];
                        string strText = string.Empty;
                        if (obj["Shape"]["Text"] != null)
                        {
                            if (json.Contains("#text"))//(json.Contains("#text"))
                            {
                                if ((obj["Shape"]["Text"]["#text"].Type.ToString()) != "Array")
                                {
                                    strText = (string)obj["Shape"]["Text"]["#text"];
                                }
                            }
                            else
                            {
                                if ((obj["Shape"]["Text"].Type.ToString()) != "Array" && (obj["Shape"]["Text"].Type.ToString()) != "Object")
                                {
                                    strText = (string)obj["Shape"]["Text"];
                                }
                            }
                        }
                        ds.Tables["Shape"].AsEnumerable().Where(row => row.Field<string>("ID") == strID)
                                .Select(b => b["TextName"] = strText).ToList();
                    }
                }

            }

            dt_Coordinate = FindOut_Coordinate(ds, xmlModel);
            dt_Coordinate.TableName = "ContainerInfo";
            ds.Tables.Add(dt_Coordinate);

            for (int i = 0; i <= dt_Coordinate.Rows.Count - 1; i++)
            {
                ds.Tables["Shape"].AsEnumerable().Where(row => row.Field<int>("Shape_Id") == Convert.ToInt32(dt_Coordinate.Rows[i]["Shape_Id"])).ToList()
                               .ForEach(b =>
                               {
                                   b["ContainerShapeId"] = dt_Coordinate.Rows[i]["ContainerShapeId"].ToString();
                                   b["IsContainer"] = dt_Coordinate.Rows[i]["IsContainer"].ToString() == string.Empty ? false : Convert.ToBoolean(dt_Coordinate.Rows[i]["IsContainer"]);
                               });
            }

            return ds;
        }

        public DataTable FindOut_Coordinate(DataSet ds, XMLModel xmlModel)
        {
            DataTable dt = new DataTable();
            double pageHeight = Convert.ToDouble(xmlModel.VisioDocument.Pages.Page.PageSheet.PageProps.PageHeight);
            double pageWidth = Convert.ToDouble(xmlModel.VisioDocument.Pages.Page.PageSheet.PageProps.PageWidth);
            double pinX = 0;
            double pinY = 0;
            double cm = 2.54;

            dt = Create_Coordinate();
            for (int i = 0; i <= ds.Tables["Shape"].Rows.Count - 1; i++)
            {
                if (ds.Tables["Shape"].Rows[i]["Name"].ToString() != string.Empty && ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() != "line")
                {
                    dt.Rows.Add();
                    dt.Rows[dt.Rows.Count - 1]["Shape_Id"] = ds.Tables["Shape"].Rows[i]["Shape_Id"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["ShapeName"] = ds.Tables["Shape"].Rows[i]["Name"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["IsContainer"] = ds.Tables["Shape"].Rows[i]["IsContainer"].ToString();

                    EnumerableRowCollection<DataRow> shapeProperty = ds.Tables["Cell"].AsEnumerable().Where(row => row.Field<int?>("Shape_Id") == Convert.ToInt32(ds.Tables["Shape"].Rows[i]["Shape_Id"].ToString())).Select(x => x);

                    var viewCell = shapeProperty.AsDataView();
                    pinX = 0;
                    pinY = 0;


                    foreach (DataRowView drv in viewCell)
                    {
                        if (drv["N"].ToString().ToLower() == "pinx")
                        {
                            pinX = Convert.ToDouble(drv["V"].ToString().ToLower());
                        }
                        else if (drv["N"].ToString().ToLower() == "piny")
                        {
                            pinY = Convert.ToDouble(drv["V"].ToString().ToLower());
                        }
                        else if (drv["N"].ToString().ToLower() == "width")
                        {
                            dt.Rows[dt.Rows.Count - 1]["Width"] = (Convert.ToDouble(drv["V"].ToString().ToLower()) * cm);
                        }
                        else if (drv["N"].ToString().ToLower() == "height")
                        {
                            dt.Rows[dt.Rows.Count - 1]["Height"] = (Convert.ToDouble(drv["V"].ToString().ToLower()) * cm);
                        }
                        else if (drv["N"].ToString().ToLower() == "locpinx")
                        {
                            dt.Rows[dt.Rows.Count - 1]["X"] = ((pinX - Convert.ToDouble(drv["V"].ToString().ToLower())) * cm);
                        }
                        else if (drv["N"].ToString().ToLower() == "locpiny")
                        {
                            dt.Rows[dt.Rows.Count - 1]["Y"] = ((pageHeight - ((pinY + Convert.ToDouble(drv["V"].ToString().ToLower())))) * cm);
                        }
                        else if (drv["N"].ToString().ToLower() == "angle")
                        {
                            dt.Rows[dt.Rows.Count - 1]["Rotation"] = Angle(drv["V"].ToString());
                        }
                    }

                    //int angleValue = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1]["Rotation"]);
                    //if(0 < angleValue)
                    //{
                    //    int x = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1]["X"]);
                    //    int y = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1]["Y"]);
                    //    var data = RotatePoint(angleValue,x,y);

                    //    dt.Rows[dt.Rows.Count - 1]["X"] = data.X;
                    //    dt.Rows[dt.Rows.Count - 1]["Y"] = data.Y;
                    //}


                }
            }

            DataTable dt_Clone = dt.Copy();

            for (int i = 0; i <= dt_Clone.Rows.Count - 1; i++)
            {
                string a = dt_Clone.Rows[i]["ShapeName"].ToString().ToLower();
                if (dt_Clone.Rows[i]["ShapeName"].ToString().ToLower() != "line")
                {
                    int x = Convert.ToInt32(dt_Clone.Rows[i]["X"]);
                    int y = Convert.ToInt32(dt_Clone.Rows[i]["y"]);
                    int height = Convert.ToInt32(dt_Clone.Rows[i]["Height"]);
                    int width = Convert.ToInt32(dt_Clone.Rows[i]["Width"]);
                    for (int j = 0; j <= dt.Rows.Count - 1; j++)
                    {
                        string b = dt.Rows[j]["ShapeName"].ToString().ToLower();
                        if (dt_Clone.Rows[i]["Shape_Id"].ToString() != dt.Rows[j]["Shape_Id"].ToString())
                        {
                            int x_point = Convert.ToInt32(dt.Rows[j]["X"]);
                            int y_point = Convert.ToInt32(dt.Rows[j]["y"]);
                            int height_point = Convert.ToInt32(dt.Rows[j]["Height"]);
                            int width_point = Convert.ToInt32(dt.Rows[j]["Width"]);

                            Rectangle rect1 = new Rectangle(x, y, width, height);
                            Rectangle rect2 = new Rectangle(x_point, y_point, width_point, height_point);
                            int angle = Convert.ToInt32(dt.Rows[j]["Rotation"]);
                            if (angle != 0)
                            {
                                Point p = RotateAngle(x_point, y_point, width_point, height_point, angle);

                                if(angle == 90 || angle == 270)
                                {
                                    int width_change = width_point;
                                    int height_change = height_point;
                                    width_point = height_point;
                                    height_point = width_change;
                                    x_point = p.X;
                                    if(angle == 90)
                                    {
                                        y_point = p.Y- width_change;
                                    }
                                    else if(angle == 270)
                                    {
                                        x_point = p.X - height_change ;
                                        y_point = p.Y - width_change;
                                    }
                                }
                                rect2 = new Rectangle(x_point, y_point, width_point, height_point);

                                if (rect1.Contains(rect2))
                                {
                                    dt.Rows[j]["ContainerShapeId"] = dt_Clone.Rows[i]["Shape_Id"];
                                    if (dt.Rows[j]["Container_ShapesId"].ToString() == string.Empty)
                                    {
                                        dt.Rows[j]["Container_ShapesId"] = dt_Clone.Rows[i]["Shape_Id"];
                                    }
                                    else
                                    {
                                        dt.Rows[j]["Container_ShapesId"] = dt.Rows[j]["Container_ShapesId"] + "|" + dt_Clone.Rows[i]["Shape_Id"];
                                    }
                                    dt.Rows[i]["IsContainer"] = true;
                                }
                            }
                            else
                            {
                                if (rect1.Contains(rect2))
                                {
                                    dt.Rows[j]["ContainerShapeId"] = dt_Clone.Rows[i]["Shape_Id"];
                                    if (dt.Rows[j]["Container_ShapesId"].ToString() == string.Empty)
                                    {
                                        dt.Rows[j]["Container_ShapesId"] = dt_Clone.Rows[i]["Shape_Id"];
                                    }
                                    else
                                    {
                                        dt.Rows[j]["Container_ShapesId"] = dt.Rows[j]["Container_ShapesId"] + "|" + dt_Clone.Rows[i]["Shape_Id"];
                                    }
                                    dt.Rows[i]["IsContainer"] = true;
                                }
                            }
                        }
                    }
                }
            }


            DataTable dtContainer = new DataTable();
            dtContainer.Columns.Add("Shape_Id", typeof(int));
            dtContainer.Columns.Add("ContainerShapeId", typeof(string));
            dtContainer.Columns.Add("Container_ShapesId", typeof(string));
            dtContainer.Columns.Add("Done", typeof(string));

            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                if (dt.Rows[i]["IsContainer"].ToString().ToLower() == "true")
                {
                    dtContainer.Rows.Add();
                    dtContainer.Rows[dtContainer.Rows.Count - 1]["Shape_Id"] = dt.Rows[i]["Shape_Id"];
                    dtContainer.Rows[dtContainer.Rows.Count - 1]["ContainerShapeId"] = dt.Rows[i]["ContainerShapeId"];
                    dtContainer.Rows[dtContainer.Rows.Count - 1]["Container_ShapesId"] = dt.Rows[i]["Container_ShapesId"];
                }
            }

            string shapeId = string.Empty;
            //for (int i = 0; i <= dtContainer.Rows.Count - 1; i++)
            //{
            //    shapeId = dtContainer.Rows[i]["Shape_Id"].ToString();
            //    if (dtContainer.Rows[i]["Done"].ToString() == string.Empty && dtContainer.Rows[i]["Container_ShapesId"].ToString() == string.Empty)
            //    {
            //        //for (int j = 0; j <= dt.Rows.Count - 1; j++)
            //        //{
            //        //    dt.Rows[j]["Container_ShapesId"] = RemoveShapeIdInContainer_ShapesId(dt, j, shapeId);
            //        //    if(!dt.Rows[j]["Container_ShapesId"].ToString().Contains("|"))
            //        //    {
            //        //        dt.Rows[j]["ContainerShapeId"] = dt.Rows[j]["Container_ShapesId"];
            //        //    }
            //        //}
            //        dtContainer.Rows[i]["Done"] = "done";
            //        dtContainer.Rows[i]["Container_ShapesId"] = string.Empty;
            //        for (int k = 0; k <= dtContainer.Rows.Count - 1; k++)
            //        {                        
            //            dtContainer.Rows[k]["Container_ShapesId"] = RemoveShapeIdInContainer_ShapesId(dtContainer, k, shapeId);                       
            //        }

            //        for (int k = 0; k <= dtContainer.Rows.Count - 1; k++)
            //        {
            //            if (!dtContainer.Rows[k]["Container_ShapesId"].ToString().Contains("|") && dtContainer.Rows[k]["Done"].ToString().ToLower() != "done")
            //            {
            //                dtContainer.Rows[k]["ContainerShapeId"] = dtContainer.Rows[k]["Container_ShapesId"];
            //                dtContainer.Rows[k]["Container_ShapesId"] = string.Empty;
            //                break;
            //            }
            //        }


            //        i = -1;
            //    }                
            //}


            for (int i = 0; i <= dtContainer.Rows.Count - 1; i++)
            {
                shapeId = dtContainer.Rows[i]["Shape_Id"].ToString();
                if (dtContainer.Rows[i]["Done"].ToString() == string.Empty && dtContainer.Rows[i]["Container_ShapesId"].ToString() == string.Empty)
                {
                    dtContainer.Rows[i]["Done"] = "done";
                    dtContainer.Rows[i]["Container_ShapesId"] = string.Empty;
                    for (int k = 0; k <= dtContainer.Rows.Count - 1; k++)
                    {
                        if (dtContainer.Rows[k]["Container_ShapesId"].ToString().Contains("|"))
                        {
                            string[] splitPipe = dtContainer.Rows[k]["Container_ShapesId"].ToString().Split('|');
                            dtContainer.Rows[k]["Container_ShapesId"] = string.Empty;

                            foreach (string pipe in splitPipe)
                            {
                                if (pipe != shapeId)
                                {
                                    if (dtContainer.Rows[k]["Container_ShapesId"].ToString() == string.Empty)
                                    {
                                        dtContainer.Rows[k]["Container_ShapesId"] = pipe;
                                    }
                                    else
                                    {
                                        dtContainer.Rows[k]["Container_ShapesId"] = dtContainer.Rows[k]["Container_ShapesId"].ToString() + "|" + pipe;
                                    }
                                }
                            }
                        }
                        else if (dtContainer.Rows[k]["Container_ShapesId"].ToString() != string.Empty)
                        {
                            dtContainer.Rows[k]["ContainerShapeId"] = dtContainer.Rows[k]["Container_ShapesId"];
                            dtContainer.Rows[k]["Container_ShapesId"] = string.Empty;
                        }
                    }

                    i = -1;
                }
            }

            return dt;
        }


        private string RemoveShapeIdInContainer_ShapesId(DataTable dt, int index, string shapeId)
        {
            string container_ShapesId = dt.Rows[index]["Container_ShapesId"].ToString();
            if (container_ShapesId.Contains("|"))
            {
                string[] splitPipe = container_ShapesId.Split('|');
                container_ShapesId = string.Empty;
                foreach (string pipe in splitPipe)
                {
                    if (pipe != shapeId)
                    {
                        if (container_ShapesId == string.Empty)
                        {
                            container_ShapesId = pipe;
                        }
                        else
                        {
                            container_ShapesId = container_ShapesId + "|" + pipe;
                        }
                    }
                }
            }
            return container_ShapesId;
        }

        private DataTable Create_Coordinate()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Shape_Id", typeof(int));
            dt.Columns.Add("ShapeName", typeof(string));
            dt.Columns.Add("IsContainer", typeof(string));
            dt.Columns.Add("ContainerShapeId", typeof(string));
            dt.Columns.Add("Container_ShapesId", typeof(string));
            dt.Columns.Add("Rotation", typeof(string));
            dt.Columns.Add("X", typeof(decimal));
            dt.Columns.Add("Y", typeof(decimal));
            dt.Columns.Add("Height", typeof(decimal));
            dt.Columns.Add("Width", typeof(decimal));

            return dt;
        }

        private int Angle(string number)
        {
            double data = number == string.Empty || number == null ? 0 : Convert.ToDouble(number);
            int anglevalue = 0;
            if (0 < data)
            {                
                anglevalue = Convert.ToInt32((data * 180) / 3.14);
            }
            if(data < 0)
            {
                anglevalue = 270;
            }
            return anglevalue;
        }


        public Point RotateAngle(int x,int y,int width,int height,int angle)
        {
            Point p = new Point();
            int Cx = x + (width / 2); // the coordinates of your center point in world coordinates
            int Cy = y + (height / 2);
            
            //The offset of a corner in local coordinates (i.e. relative to the pivot point)
            //(which corner will depend on the coordinate reference system used in your environment)
            int Ox = width / 2;
            int Oy = height / 2;

            //The rotated position of this corner in world coordinates    
            p.X = Convert.ToInt32(Cx + (Ox * Math.Cos(angle)) - (Oy * Math.Sin(angle)));
            p.Y = Convert.ToInt32(Cy + (Ox * Math.Sin(angle)) + (Oy * Math.Cos(angle)));
            return p;
        }

    }
}
