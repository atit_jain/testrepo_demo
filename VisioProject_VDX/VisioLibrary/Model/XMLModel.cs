﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisioLibrary
{

    public class XMLModel
    {
        public Visiodocument VisioDocument { get; set; }
    }

    public class Visiodocument
    {
        //public Facenames FaceNames { get; set; }
        //public Masters Masters { get; set; }
        public Pages Pages { get; set; }
        //public string _xmlspace { get; set; }
    }

    public class Facenames
    {
        public Facename[] FaceName { get; set; }
    }

    public class Facename
    {
        public string _CharSets { get; set; }
        public string _Flags { get; set; }
        public string _ID { get; set; }
        public string _Name { get; set; }
        public string _Panos { get; set; }
        public string _UnicodeRanges { get; set; }
    }

    public class Masters
    {
        public Master[] Master { get; set; }
    }

    public class Master
    {
        public Shapes Shapes { get; set; }
        public string _ID { get; set; }
        public string _Name { get; set; }
        public string _NameU { get; set; }
    }

    public class Shapes
    {
        public Shape Shape { get; set; }
    }

    public class Shape
    {
        public Xform XForm { get; set; }
        public Textxform TextXForm { get; set; }
        public Fill Fill { get; set; }
        public Line Line { get; set; }
        public Protection Protection { get; set; }
        public Group Group { get; set; }
        public Event Event { get; set; }
        public Char Char { get; set; }
        public Para Para { get; set; }
        public Connection[] Connection { get; set; }
        public Shapes1 Shapes { get; set; }
        public Textblock TextBlock { get; set; }
        public Text Text { get; set; }
        public Property1 Property { get; set; }
        public string _ID { get; set; }
        public string _Type { get; set; }
    }

    public class Xform
    {
        public string Width { get; set; }
        public string Height { get; set; }
        public string PinX { get; set; }
        public string PinY { get; set; }
        public Locpinx LocPinX { get; set; }
        public Locpiny LocPinY { get; set; }
        public string Angle { get; set; }
    }

    public class Locpinx
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Locpiny
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Textxform
    {
        public Txtwidth TxtWidth { get; set; }
        public Txtheight TxtHeight { get; set; }
        public Txtpinx TxtPinX { get; set; }
        public Txtpiny TxtPinY { get; set; }
        public Txtlocpinx TxtLocPinX { get; set; }
        public Txtlocpiny TxtLocPinY { get; set; }
        public string TxtAngle { get; set; }
    }

    public class Txtwidth
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Txtheight
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Txtpinx
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Txtpiny
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Txtlocpinx
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Txtlocpiny
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Fill
    {
        public string FillForegnd { get; set; }
        public string FillBkgnd { get; set; }
        public string FillForegndTrans { get; set; }
        public string FillBkgndTrans { get; set; }
        public string FillPattern { get; set; }
    }

    public class Line
    {
        public string LineWeight { get; set; }
        public string LineColor { get; set; }
        public string LineColorTrans { get; set; }
        public string LinePattern { get; set; }
        public string EndArrowSize { get; set; }
        public string BeginArrowSize { get; set; }
        public string BeginArrow { get; set; }
        public string EndArrow { get; set; }
        public string Rounding { get; set; }
    }

    public class Protection
    {
        public string LockGroup { get; set; }
        public string LockTextEdit { get; set; }
    }

    public class Group
    {
        public string DontMoveChildren { get; set; }
    }

    public class Event
    {
        public Eventdblclick EventDblClick { get; set; }
    }

    public class Eventdblclick
    {
        public string _F { get; set; }
    }

    public class Char
    {
        public string Font { get; set; }
        public string ComplexScriptFont { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public string Style { get; set; }
        public string Case { get; set; }
        public string Pos { get; set; }
        public string Strikethru { get; set; }
        public string ColorTrans { get; set; }
        public string _IX { get; set; }
    }

    public class Para
    {
        public string IndFirst { get; set; }
        public string IndLeft { get; set; }
        public string IndRight { get; set; }
        public Spline SpLine { get; set; }
        public string SpBefore { get; set; }
        public string SpAfter { get; set; }
        public string HorzAlign { get; set; }
        public string Bullet { get; set; }
        public string _IX { get; set; }
    }

    public class Spline
    {
        public string _Unit { get; set; }
        public string __text { get; set; }
    }

    public class Shapes1
    {
        public object Shape { get; set; }
    }

    public class Textblock
    {
        public string VerticalAlign { get; set; }
        public string LeftMargin { get; set; }
        public string RightMargin { get; set; }
        public string TopMargin { get; set; }
        public string BottomMargin { get; set; }
    }

    public class Text
    {
        public Pp pp { get; set; }
        public Cp cp { get; set; }
        public string __text { get; set; }
    }

    public class Pp
    {
        public string _IX { get; set; }
    }

    public class Cp
    {
        public string _IX { get; set; }
    }

    public class Property1
    {
        public string _xmlnslc { get; set; }
        public string _Name { get; set; }
        public string __prefix { get; set; }
        public string __text { get; set; }
    }

    public class Connection
    {
        public X X { get; set; }
        public Y Y { get; set; }
        public string _IX { get; set; }
    }

    public class X
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Y
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Pages
    {
        public Page Page { get; set; }
    }

    public class Page
    {
        public Pagesheet PageSheet { get; set; }
        //public Shapes2 Shapes { get; set; }
        //public Connects Connects { get; set; }
        //public string _ID { get; set; }
        //public string _Name { get; set; }
    }

    public class Pagesheet
    {
        public Pageprops PageProps { get; set; }
        public Pagelayout PageLayout { get; set; }
        public Printprops PrintProps { get; set; }
    }

    public class Pageprops
    {
        public string PageWidth { get; set; }
        public string PageHeight { get; set; }
        public string DrawingScale { get; set; }
        public string PageScale { get; set; }
        public string DrawingScaleType { get; set; }
    }

    public class Pagelayout
    {
        public string LineJumpCode { get; set; }
    }

    public class Printprops
    {
        public string PageLeftMargin { get; set; }
        public string PageRightMargin { get; set; }
        public string PageTopMargin { get; set; }
        public string PageBottomMargin { get; set; }
        public string PaperKind { get; set; }
        public string PrintPageOrientation { get; set; }
        public string ScaleX { get; set; }
        public string ScaleY { get; set; }
    }

    public class Shapes2
    {
        public Shape1[] Shape { get; set; }
    }

    public class Shape1
    {
        public Xform1 XForm { get; set; }
        public Fill1 Fill { get; set; }
        public Line1 Line { get; set; }
        public Protection1 Protection { get; set; }
        public object Group { get; set; }
        public object Event { get; set; }
        public Connection1[] Connection { get; set; }
        public Shapes3 Shapes { get; set; }
        public Property2[] Property { get; set; }
        public string _ID { get; set; }
        public string _Master { get; set; }
        public string _Name { get; set; }
        public string _NameU { get; set; }
        public string _Type { get; set; }
        public Textxform1 TextXForm { get; set; }
        public Char1 Char { get; set; }
        public Para1 Para { get; set; }
        public Textblock1 TextBlock { get; set; }
        public Text1 Text { get; set; }
        public Xform1d XForm1D { get; set; }
        public Misc Misc { get; set; }
        public Layout Layout { get; set; }
        public Geom Geom { get; set; }
    }

    public class Xform1
    {
        public string Width { get; set; }
        public string Height { get; set; }
        public string PinX { get; set; }
        public string PinY { get; set; }
        public Locpinx1 LocPinX { get; set; }
        public Locpiny1 LocPinY { get; set; }
        public string Angle { get; set; }
    }

    public class Locpinx1
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Locpiny1
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Fill1
    {
        public string FillForegnd { get; set; }
        public string FillBkgnd { get; set; }
        public string FillForegndTrans { get; set; }
        public string FillBkgndTrans { get; set; }
        public string FillPattern { get; set; }
    }

    public class Line1
    {
        public string LineWeight { get; set; }
        public string LineColor { get; set; }
        public string LineColorTrans { get; set; }
        public string LinePattern { get; set; }
        public string EndArrowSize { get; set; }
        public string BeginArrowSize { get; set; }
        public string BeginArrow { get; set; }
        public string EndArrow { get; set; }
        public string Rounding { get; set; }
    }

    public class Protection1
    {
        public string LockGroup { get; set; }
        public string LockTextEdit { get; set; }
    }

    public class Shapes3
    {
        public object Shape { get; set; }
    }

    public class Textxform1
    {
        public Txtwidth1 TxtWidth { get; set; }
        public Txtheight1 TxtHeight { get; set; }
        public Txtpinx1 TxtPinX { get; set; }
        public Txtpiny1 TxtPinY { get; set; }
        public Txtlocpinx1 TxtLocPinX { get; set; }
        public Txtlocpiny1 TxtLocPinY { get; set; }
        public string TxtAngle { get; set; }
    }

    public class Txtwidth1
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Txtheight1
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Txtpinx1
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Txtpiny1
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Txtlocpinx1
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Txtlocpiny1
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Char1
    {
        public string Font { get; set; }
        public string ComplexScriptFont { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public string Style { get; set; }
        public string Case { get; set; }
        public string Pos { get; set; }
        public string Strikethru { get; set; }
        public string ColorTrans { get; set; }
        public string _IX { get; set; }
    }

    public class Para1
    {
        public string IndFirst { get; set; }
        public string IndLeft { get; set; }
        public string IndRight { get; set; }
        public Spline1 SpLine { get; set; }
        public string SpBefore { get; set; }
        public string SpAfter { get; set; }
        public string HorzAlign { get; set; }
        public string Bullet { get; set; }
        public string _IX { get; set; }
    }

    public class Spline1
    {
        public string _Unit { get; set; }
        public string __text { get; set; }
    }

    public class Textblock1
    {
        public string VerticalAlign { get; set; }
        public string LeftMargin { get; set; }
        public string RightMargin { get; set; }
        public string TopMargin { get; set; }
        public string BottomMargin { get; set; }
    }

    public class Text1
    {
        public Pp1 pp { get; set; }
        public Cp1 cp { get; set; }
        public string __text { get; set; }
    }

    public class Pp1
    {
        public string _IX { get; set; }
    }

    public class Cp1
    {
        public string _IX { get; set; }
    }

    public class Xform1d
    {
        public string BeginX { get; set; }
        public string BeginY { get; set; }
        public string EndX { get; set; }
        public string EndY { get; set; }
    }

    public class Misc
    {
        public string ObjType { get; set; }
        public string DynFeedback { get; set; }
        public string NoLiveDynamics { get; set; }
        public string NoAlignBox { get; set; }
    }

    public class Layout
    {
        public string ConFixedCode { get; set; }
        public string ShapeRouteStyle { get; set; }
        public string ConLineRouteExt { get; set; }
        public string ConLineJumpCode { get; set; }
    }

    public class Geom
    {
        public string NoFill { get; set; }
        public string NoLine { get; set; }
        public string NoShow { get; set; }
        public Moveto MoveTo { get; set; }
        public Lineto[] LineTo { get; set; }
        public string _IX { get; set; }
    }

    public class Moveto
    {
        public X1 X { get; set; }
        public Y1 Y { get; set; }
        public string _IX { get; set; }
    }

    public class X1
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Y1
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Lineto
    {
        public X2 X { get; set; }
        public Y2 Y { get; set; }
        public string _IX { get; set; }
    }

    public class X2
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Y2
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Connection1
    {
        public X3 X { get; set; }
        public Y3 Y { get; set; }
        public string _IX { get; set; }
    }

    public class X3
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Y3
    {
        public string _F { get; set; }
        public string __text { get; set; }
    }

    public class Property2
    {
        public string _xmlnslc { get; set; }
        public string _Name { get; set; }
        public string __prefix { get; set; }
        public string __text { get; set; }
    }

    public class Connects
    {
        public Connect[] Connect { get; set; }
    }

    public class Connect
    {
        public string _FromPart { get; set; }
        public string _FromSheet { get; set; }
        public string _ToPart { get; set; }
        public string _ToSheet { get; set; }
    }

}
