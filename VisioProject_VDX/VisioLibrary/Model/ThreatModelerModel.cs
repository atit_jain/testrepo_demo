﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisioLibrary
{
    public class ThreatModelerModel
    {
        public List<NodeDataArray_Data> nodeDataArray = new List<NodeDataArray_Data>();
        public List<LinkDataArray_Data> linkDataArray = new List<LinkDataArray_Data>();
    }

    #region NodeDataArray
    public class NodeDataArray_Data
    {
        public int Id { get; set; }
        public int key { get; set; }
        public string category { get; set; }
        public string Name { get; set; }
        public string UniqueID { get; set; }
        public string Type { get; set; }
        public string ImageURI { get; set; }
        public int NodeId { get; set; }
        public int Group { get; set; }
        public string Location { get; set; }
        public string BackgroundColor { get; set; }
        public string BorderColor { get; set; }
        public string TitleColor { get; set; }
        public string TitleBackgroundColor { get; set; }
        public bool isGroup { get; set; }
        public string isGraphExpanded { get; set; }
        public string FullName { get; set; }
        public string cpeid { get; set; }
        public int networkComponentId { get; set; }
        public decimal BorderThickness { get; set; }

        public string guid { get; set; }
        //Exta Property
        public string ForeColor { get; set; }
    }
    #endregion

    #region LinkDataArray
    public class LinkDataArray_Data
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public int from { get; set; }
        public int to { get; set; }
        public bool IsVisible { get; set; }
        public int corner { get; set; }

        public string connecter { get; set; }
        public string guid { get; set; }
    }
    #endregion

    public class CsvFile
    {
      public string Value { get; set; }
      public string Comment { get; set; }
      public string GuId { get; set; }
    }
        
    public class ThreatModels
    {
        public List<NodeDataArray> nodeDataArray = new List<NodeDataArray>();
        public List<LinkDataArray> linkDataArray = new List<LinkDataArray>();
    }

    public class NodeDataArray
    {
        public NodeDataArray(NodeDataArray_Data node)
        {
            key = node.key + 1;
            category = node.category;
            Name = node.Name;
            Type = node.Type;
            Group = node.Group + 1;
            Location = node.Location.Replace(",","");
            BackgroundColor = String.IsNullOrEmpty(node.BackgroundColor) ? "transparent" : node.BackgroundColor;
            BorderColor = node.BorderColor;
            TitleColor = String.IsNullOrEmpty(node.TitleColor)  ? "black" : node.TitleColor;
            TitleBackgroundColor = String.IsNullOrEmpty(node.TitleBackgroundColor) ? "transparent" : node.TitleBackgroundColor;
            isGroup = node.isGroup;
            isGraphExpanded = "false";
            FullName = String.IsNullOrEmpty(node.FullName) ? Name : node.FullName.ToString();
            BorderThickness = node.isGroup ? 1 : 0;
            guid = node.guid;
        }

        public int key { get; set; } //It has to be positive number.starting from 1.
        public string Name { get; set; }
        public string FullName { get; set; }
        public bool isGroup { get; set; } // if Group then true, otherwise false
        public int Group { get; set; } // If node is inside the Group, then set Parent value in this field.
        public string category { get; set; } // "Component" when its a Node, OR "Collection" when its a Group.
        public string Type { get; set; } //It can be a "Node" of "Group"
        public int BorderThickness { get; set; } // If group then 1 otherwise 0
        public string TitleColor { get; set; } // black
        public string TitleBackgroundColor { get; set; } // transparent
        public string BackgroundColor { get; set; } // transparent
        public string Location { get; set; }
        public string isGraphExpanded { get; set; } //false
        public string BorderColor { get; set; }
        public string guid { get; set; }
    }

    public class LinkDataArray
    {
        public LinkDataArray(LinkDataArray_Data link)
        {
            protocolIds = new List<int>();
            Name = String.IsNullOrEmpty(link.Name) ? "HTTPS" : link.Name;
            from = link.from + 1;
            to = link.to + 1;
            strokeWidth = 1;
            toArrow = "OpenTriangle";
            Color = String.IsNullOrEmpty(link.Name) ? "#078181" : link.Color;
            protocolIds.Add(488);
        }

        public int from { get; set; } // key of source
        public int to { get; set; } // key of target
        public List<int> protocolIds { get; set; } //[488]
        public string Name { get; set; } // Text written on Link (If not pass HTTPS)
        public string toArrow { get; set; } // "OpenTriangle"        
        public int strokeWidth { get; set; } // 1              
        public string Color { get; set; } //#078181
    }
}
