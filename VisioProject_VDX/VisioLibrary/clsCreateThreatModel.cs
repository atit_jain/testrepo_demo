﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Aspose.Diagram;
using System.Windows.Forms;
using System.Configuration;
using System.Xml;
using Newtonsoft.Json;

namespace VisioLibrary
{
    public class clsCreateThreatModel
    {
     

        public string CreateJson(string directoryName, string fileName)
        {
            string oldFile = fileName;
            string oldDirectoryName = directoryName;
            string dummyFileName = "drawing56823.vsdx";
            string fileExtension = string.Empty;
            bool isOtherFile = false;
            string jsonText = string.Empty;
            DataSet ds = new DataSet();
            ThreatModelerModel threadModel = null;
            FileStream LicStream = null;
            License license = null;
            try
            {
                //Metered metered = new Metered();               
                //metered.SetMeteredKey("your-public-key", "your-private-key");
                logClass.WriteLog(directoryName, "-------------------------------------------------------------");

                string dataDir = directoryName + "\\" + "Aspose.Diagram.lic";

               
                    //using(LicStream = new FileStream(dataDir, FileMode.Open))
                    // {
                    license = new License();
                    license.SetLicense(dataDir);
                // }

                logClass.WriteLog(directoryName, "license.done");

                clsReadVisioFile.strNotFoundComponentData = string.Empty;
                fileExtension = System.IO.Path.GetExtension(fileName);
                XMLModel xmlModel = XMLToModel(directoryName, fileName);
                if (!clsReadVisioFile.isWithoutConvertFile)
                {
                    if (fileExtension.ToLower() == clsConstant.Extension_vdx || fileExtension.ToLower() == clsConstant.Extension_vsdx
                        || fileExtension.ToLower() == clsConstant.Extension_vsd || fileExtension.ToLower() == clsConstant.Extension_vss
                        || fileExtension.ToLower() == clsConstant.Extension_vst)
                    {
                        if (File.Exists(directoryName + @"\" + dummyFileName))
                        {
                            File.Delete(directoryName + @"\" + dummyFileName);
                        }
                        string _filename = fileName;

                        var diagram = new Aspose.Diagram.Diagram(directoryName + @"\" + _filename);
                        fileName = dummyFileName;
                        diagram.Save(directoryName + @"\" + fileName, SaveFileFormat.VSDX);
                        isOtherFile = true;
                        logClass.WriteLog(directoryName, "convert vdx file");
                    }
                }


                ds = new clsReadVisioFile().CreateDataSetFromVisio(fileName, directoryName, xmlModel);
                logClass.WriteLog(directoryName, "generate dataset");
                threadModel = new clsCreateThreatModel().CreateThreatModel(ds);
                logClass.WriteLog(directoryName, "generate thread Model from dataset");
                ThreatModels threatModels = new ThreatModels();
                threadModel.nodeDataArray.ForEach(s => threatModels.nodeDataArray.Add(new NodeDataArray(s)));
                threadModel.linkDataArray.ForEach(s => threatModels.linkDataArray.Add(new LinkDataArray(s)));

                jsonText = Newtonsoft.Json.JsonConvert.SerializeObject(threatModels);
                logClass.WriteLog(directoryName, "generate Json file");
                logClass.WriteLog(directoryName, "-------------------------------------------------------------");
                clsReadVisioFile.strJsonData = jsonText;
                return jsonText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                LicStream = null;
                license = null;
                if (isOtherFile)
                {
                    if (File.Exists(directoryName + @"\" + fileName))
                    {
                        File.Delete(directoryName + @"\" + fileName);
                    }
                }
            }
        }

        private XMLModel XMLToModel(string directoryName, string fileName)
        {
            string filepath = directoryName + "\\" + fileName;
            string document = string.Empty;
            string json = string.Empty;

            using (System.IO.StreamReader sr = new System.IO.StreamReader(filepath))
            {
                document = sr.ReadLine();
                document = document.Replace("\"@", "\"");
            }

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(document);
            json = JsonConvert.SerializeXmlNode(doc);
            document = json.Replace("\"@", "\"");

            return JsonConvert.DeserializeObject<XMLModel>(document);
        }

        internal Dictionary<string, CsvFile> GetDict(DataTable dt)
        {
            Dictionary<string, CsvFile> dic = new Dictionary<string, CsvFile>();

            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                dic.Add(dt.Rows[i]["Name"].ToString(), new CsvFile
                {
                    Value = dt.Rows[i]["Value"].ToString(),
                    Comment = dt.Rows[i]["Comment"].ToString(),
                    GuId = dt.Rows[i]["GuId"].ToString()
                });
            }

            return dic;
        }

        private ThreatModelerModel CreateThreatModel(DataSet ds)
        {
            ThreatModelerModel threadModel = new ThreatModelerModel();
            KeyValuePair<string, CsvFile> dataResource = new KeyValuePair<string, CsvFile>();
            KeyValuePair<string, CsvFile> dataResource_Text = new KeyValuePair<string, CsvFile>();
            EnumerableRowCollection<DataRow> getConnect = null;
            DataView viewConnect = null;
            DataRow getDataRow = null;
            bool isAddLink = false;
            List<NodeDataArray_Data> nodeDataArray = new List<NodeDataArray_Data>();
            List<LinkDataArray_Data> linkDataArray = new List<LinkDataArray_Data>();
            try
            {
                var dataResourceFile = GetDict(GetDataTabletFromCSVFile(ConfigurationManager.AppSettings["Resource_Mapping"]));
                var dataResourceFile_Text = GetDict(GetDataTabletFromCSVFile(ConfigurationManager.AppSettings["Resource_Text"]));

                ds = SetAllNameByMaster(ds);
                for (int i = 0; i <= ds.Tables["Shape"].Rows.Count - 1; i++)
                {
                    if (ds.Tables["Shape"].Rows[i]["Name"].ToString() != string.Empty || ds.Tables["Shape"].Rows[i]["TextName"].ToString() != string.Empty)
                    {
                        if (ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() != clsConstant.DynamicConnector &&
                           ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() != clsConstant.OneWayDataConnection &&
                           ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() != clsConstant.TwoWayDataConnection
                         && ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() != "line")
                        {
                            //string aa = ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower();
                            dataResource_Text = new KeyValuePair<string, CsvFile>();
                            dataResource = new KeyValuePair<string, CsvFile>();
                            if (ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() == clsConstant_Shape.Foreign)
                            {
                                dataResource_Text = dataResourceFile_Text.Where(x => x.Key.ToLower().Replace(" ", string.Empty) == ds.Tables["Shape"].Rows[i]["TextName"].ToString().Replace(" ", string.Empty).Trim().ToLower())
                                                .FirstOrDefault();
                                if (dataResource_Text.Key == null)
                                {
                                    dataResource = dataResourceFile.Where(x => x.Key.ToLower().Replace(" ", string.Empty) == ds.Tables["Shape"].Rows[i]["TextName"].ToString().Replace(" ", string.Empty).Trim().ToLower())
                                                .FirstOrDefault();
                                }
                            }
                            else
                            {
                                dataResource_Text = dataResourceFile_Text.Where(x => x.Key.ToLower().Replace(" ", string.Empty) == ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower()
                                   || x.Key.ToLower().Replace(" ", string.Empty) == ds.Tables["Shape"].Rows[i]["TextName"].ToString().Replace(" ", string.Empty).Trim().ToLower()).FirstOrDefault();
                                if (dataResource_Text.Key == null)
                                {
                                    dataResource = dataResourceFile.Where(x => x.Key.ToLower().Replace(" ", string.Empty) == ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower()
                                   || x.Key.ToLower().Replace(" ", string.Empty) == ds.Tables["Shape"].Rows[i]["TextName"].ToString().Replace(" ", string.Empty).Trim().ToLower()).FirstOrDefault();
                                }
                            }

                            if (dataResource.Key != null || dataResource_Text.Key != null)
                            {
                                string fullName = ds.Tables["Shape"].Rows[i]["TextName"].ToString();
                                ds.Tables["Shape"].Rows[i]["ThreadShape"] = true;
                                nodeDataArray.Add(ReturnNodeDataArray(ds, i, dataResource, dataResource_Text, fullName));
                            }
                            else
                            {
                                ds.Tables["Shape"].Rows[i]["ThreadShape"] = false;
                                if (clsReadVisioFile.strNotFoundComponentData == string.Empty)
                                {
                                    clsReadVisioFile.strNotFoundComponentData = "Component Name (Text Value)";
                                    clsReadVisioFile.strNotFoundComponentData += Environment.NewLine + ds.Tables["Shape"].Rows[i]["Name"].ToString() + " (" + ds.Tables["Shape"].Rows[i]["TextName"].ToString() + ")";
                                }
                                else
                                {
                                    clsReadVisioFile.strNotFoundComponentData += Environment.NewLine + ds.Tables["Shape"].Rows[i]["Name"].ToString() + " (" + ds.Tables["Shape"].Rows[i]["TextName"].ToString() + ")";
                                }
                            }
                        }
                    }
                }

                for (int i = 0; i <= ds.Tables["Shape"].Rows.Count - 1; i++)
                {
                    if (ds.Tables["Shape"].Rows[i]["Name"].ToString() != string.Empty)
                    {
                        if (ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() == clsConstant.DynamicConnector ||
                           ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() == clsConstant.OneWayDataConnection ||
                           ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() == clsConstant.TwoWayDataConnection ||
                           ds.Tables["Shape"].Rows[i]["Name"].ToString().ToLower() == "line")
                        {
                            isAddLink = false;
                            getConnect = ds.Tables["Connect"].AsEnumerable().Where(row => row.Field<string>
                                  ("FromSheet") == ds.Tables["Shape"].Rows[i]["ID"].ToString()).Select(x => x);

                            viewConnect = getConnect.AsDataView();
                            foreach (DataRowView drv in viewConnect)
                            {
                                getDataRow = ds.Tables["Shape"].AsEnumerable().Where(row => row.Field<string>
                                                    ("ID") == drv["ToSheet"].ToString()).Select(x => x).FirstOrDefault();

                                isAddLink = Convert.ToBoolean(getDataRow["ThreadShape"].ToString() == string.Empty ? false : getDataRow["ThreadShape"]) ? true : false;
                                if (isAddLink == false)
                                    break;
                            }

                            if (isAddLink == true)
                                linkDataArray.Add(ReturnLinkDataArray(ds, i));
                        }
                    }
                }
                threadModel.nodeDataArray = nodeDataArray;
                threadModel.linkDataArray = linkDataArray;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                getConnect = null;
                viewConnect = null;
                getDataRow = null;
            }
            return threadModel;
        }

        private DataTable GetDataTabletFromCSVFile(string csv_file_path)
        {
            DataTable csvData = new DataTable();
            try
            {
                using (Microsoft.VisualBasic.FileIO.TextFieldParser csvReader = new Microsoft.VisualBasic.FileIO.TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    //read column names
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        System.Data.DataColumn datecolumn = new System.Data.DataColumn(column);
                        datecolumn.AllowDBNull = true;
                        csvData.Columns.Add(datecolumn);
                    }
                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        //Making empty value as null
                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = null;
                            }
                        }
                        csvData.Rows.Add(fieldData);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return csvData;
        }

        private DataSet SetAllNameByMaster(DataSet ds)
        {
            //do not remve this loop because some time name not fount in first shape
            for (int i = 0; i <= ds.Tables["Shape"].Rows.Count - 1; i++)
            {
                if (ds.Tables["Shape"].Rows[i]["Type"].ToString().ToLower() == clsConstant_Shape.Foreign)
                {
                    ds.Tables["Shape"].Rows[i]["Name"] = clsConstant_Shape.Foreign;
                    ds.Tables["Shape"].Rows[i]["NameU"] = clsConstant_Shape.Foreign;
                    ds.Tables["Shape"].Rows[i]["Master"] = "5682";
                }

                if (ds.Tables["Shape"].Rows[i]["NameU"].ToString() != string.Empty)
                {
                    ds.Tables["Shape"].AsEnumerable().Where(row => row.Field<string>("Master") == ds.Tables["Shape"].Rows[i]["Master"].ToString())
                    .Select(b => b["Name"] = ds.Tables["Shape"].Rows[i]["Name"].ToString().Replace(" ", string.Empty))
                    .ToList();
                }
            }
            return ds;
        }

        private NodeDataArray_Data ReturnNodeDataArray(DataSet ds, int index, KeyValuePair<string, CsvFile> dataResource, KeyValuePair<string, CsvFile> dataResource_Text, string fullName)
        {
            NodeDataArray_Data nodeDataArray = new NodeDataArray_Data();
            EnumerableRowCollection<DataRow> getCell = null;
            DataView viewCell = null;
            double pinx = 0;
            double piny = 0;
            try
            {
                if (dataResource.Key == clsConstant_Shape.Foreign)
                {
                    nodeDataArray.ImageURI = dataResource.Value.Value;
                    nodeDataArray.FullName = fullName;
                    nodeDataArray.guid = dataResource.Value.GuId;
                }
                else
                {
                    nodeDataArray.FullName = fullName;
                    nodeDataArray.Name = dataResource_Text.Key != null ? dataResource_Text.Value.Value : dataResource.Value.Value;
                    nodeDataArray.guid = dataResource_Text.Key != null ? dataResource_Text.Value.GuId : dataResource.Value.GuId;
                }

                nodeDataArray.Id = Convert.ToInt32(ds.Tables["Shape"].Rows[index]["Shape_Id"].ToString());
                nodeDataArray.key = Convert.ToInt32(ds.Tables["Shape"].Rows[index]["Shape_Id"].ToString());
                nodeDataArray.Group = ds.Tables["Shape"].Rows[index]["ContainerShapeId"].ToString() == string.Empty ? -1 : Convert.ToInt32(ds.Tables["Shape"].Rows[index]["ContainerShapeId"]);
                nodeDataArray.isGroup = ds.Tables["Shape"].Rows[index]["IsContainer"].ToString() == string.Empty || ds.Tables["Shape"].Rows[index]["IsContainer"].ToString().ToLower() == "false" ? false : Convert.ToBoolean(ds.Tables["Shape"].Rows[index]["IsContainer"]);
                nodeDataArray.Type = ds.Tables["Shape"].Rows[index]["IsContainer"].ToString() == string.Empty || ds.Tables["Shape"].Rows[index]["IsContainer"].ToString().ToLower() == "false" ? "Node" : "Group";
                nodeDataArray.category = ds.Tables["Shape"].Rows[index]["IsContainer"].ToString() == string.Empty || ds.Tables["Shape"].Rows[index]["IsContainer"].ToString().ToLower() == "false" ? "Component" : "Collection";

                getCell = ds.Tables["Cell"].AsEnumerable().Where(row => row.Field<int?>
                                      ("Shape_Id") == Convert.ToInt32(ds.Tables["Shape"].Rows[index]["Shape_id"].ToString())).Select(x => x);

                for (int i = 0; i <= ds.Tables["ContainerInfo"].Rows.Count - 1; i++)
                {
                    if (ds.Tables["ContainerInfo"].Rows[i]["Shape_Id"].ToString() == nodeDataArray.key.ToString())
                    {
                        pinx = Convert.ToDouble(ds.Tables["ContainerInfo"].Rows[i]["X"]);
                        piny = Convert.ToDouble(ds.Tables["ContainerInfo"].Rows[i]["Y"]);
                        break;
                    }
                }


                viewCell = getCell.AsDataView();

                foreach (DataRowView drv in viewCell)
                {
                    if (drv["N"].ToString().ToLower() == clsConstant.PinX)
                    {
                        nodeDataArray.Location = (pinx * clsConstant.distance).ToString("n2") + " ";
                    }
                    else if (drv["N"].ToString().ToLower() == clsConstant.PinY)
                    {
                        nodeDataArray.Location += (piny * clsConstant.distance).ToString("n2");
                    }
                    else if (drv["N"].ToString().ToLower() == clsConstant.FillForegnd)
                    {
                        nodeDataArray.ForeColor += drv["V"].ToString();
                    }
                    else if (drv["N"].ToString().ToLower() == clsConstant.FillForegnd)
                    {
                        nodeDataArray.TitleColor += drv["V"].ToString();
                    }
                    else if (drv["N"].ToString().ToLower() == clsConstant.Fillbkgnd)
                    {
                        nodeDataArray.BackgroundColor += drv["V"].ToString();
                    }
                    else if (drv["N"].ToString().ToLower() == clsConstant.LineColor)
                    {
                        nodeDataArray.BorderColor += drv["V"].ToString();
                    }
                    else if (drv["N"].ToString().ToLower() == clsConstant.LineWeight)
                    {
                        nodeDataArray.BorderThickness = Convert.ToDecimal(drv["V"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                getCell = null;
                viewCell = null;
            }
            return nodeDataArray;
        }

        private LinkDataArray_Data ReturnLinkDataArray(DataSet ds, int index)
        {
            LinkDataArray_Data linkDataArray = new LinkDataArray_Data();
            EnumerableRowCollection<DataRow> getCell = null;
            EnumerableRowCollection<DataRow> getConnect = null;
            DataView viewConnect = null;
            DataView viewCell = null;
            bool isBeginArrow = false;
            bool isTwoWayDirection = false;
            try
            {
                linkDataArray.Name = ds.Tables["Shape"].Rows[index]["TextName"].ToString().ToLower();                
                linkDataArray.IsVisible = true;

                getCell = ds.Tables["Cell"].AsEnumerable().Where(row => row.Field<int?>
                                      ("Shape_Id") == Convert.ToInt32(ds.Tables["Shape"].Rows[index]["Shape_id"].ToString())).Select(x => x);

                viewCell = getCell.AsDataView();
                foreach (DataRowView drv in viewCell)
                {
                    if (drv["N"].ToString().ToLower() == clsConstant.LineColor)
                    {
                        linkDataArray.Color = drv["V"].ToString();
                    }

                    if (ds.Tables["Shape"].Rows[index]["Name"].ToString().ToLower() == clsConstant.TwoWayDataConnection.ToLower())
                    {
                        isTwoWayDirection = true;
                    }
                    else
                    {
                        if (drv["N"].ToString().ToLower() == clsConstant.BeginArrow || drv["N"].ToString().ToLower() == clsConstant.EndArrow)
                        {

                            if (drv["V"].ToString().ToLower() != string.Empty)
                            {
                                if (drv["N"].ToString().ToLower() == clsConstant.BeginArrow)
                                {
                                    isTwoWayDirection = Convert.ToInt32(drv["V"].ToString()) != 0 ? true : false;
                                    isBeginArrow = true;
                                }
                                else
                                {
                                    if (isBeginArrow)
                                    {
                                        isTwoWayDirection = Convert.ToInt32(drv["V"].ToString()) != 0 ? true : false;
                                    }
                                }
                            }
                        }
                    }
                }

                if (isTwoWayDirection)
                {
                    linkDataArray.connecter = "Bi directional";
                }
                else
                {
                    linkDataArray.connecter = "unidirectional";
                }
                getConnect = ds.Tables["Connect"].AsEnumerable().Where(row => row.Field<string>
                                      ("FromSheet") == ds.Tables["Shape"].Rows[index]["ID"].ToString()).Select(x => x);

                viewConnect = getConnect.AsDataView();

                foreach (DataRowView drv in viewConnect)
                {
                    if (drv["FromPart"].ToString().ToLower() == "9") 
                    {
                        var shapeId = ds.Tables["Shape"].AsEnumerable().Where(row => row.Field<string>
                                       ("ID") == drv["ToSheet"].ToString()).ToList();

                        linkDataArray.from = Convert.ToInt32((shapeId[0]).ItemArray[0]);
                    }
                    else if (drv["FromPart"].ToString().ToLower() == "12")
                    {
                        var shapeId = ds.Tables["Shape"].AsEnumerable().Where(row => row.Field<string>
                                       ("ID") == drv["ToSheet"].ToString()).ToList();
                        linkDataArray.to = Convert.ToInt32((shapeId[0]).ItemArray[0]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                getCell = null;
                getConnect = null;
                viewConnect = null;
                viewCell = null;
            }
            return linkDataArray;
        }

    }
}
